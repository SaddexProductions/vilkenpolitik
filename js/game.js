$(document).ready(function () {
    let myGame = new Game();
    myGame.init('gameArea');
    $("#imgWrapper").html("<a href='https://www.saddexproductions.com' target='_blank' rel='noopener noreferrer'><img src='img/logo.png'></a>");
    let urlLocation = window.location.href;
    if (urlLocation.indexOf("www") > -1) {
        window.location.replace("https://vilkenpolitik.se")
    }
});

function onloadCallback() {
    grecaptcha.render('captcha', {
        'sitekey': '6LdEeX8UAAAAADmJUFAsdjMl235HALGAhjMw85Et'
    })
}
async function convertError(err) {
    return new Promise(resolve => {
        let extracted = "";
        if (err.status) {
            err.json().then(data => {
                extracted = data.msg;
                return resolve(extracted)
            }).catch(_ => {
                extracted = `${err.status} ${err.statusText}`;
                return resolve(extracted)
            })
        } else {
            return resolve("No connection, or the operation was aborted")
        }
    })
}

function Game() {
    const v = 20,
        vMax = 500,
        decay = 0.8,
        c = 60,
        c2 = 20,
        mS = -500,
        scorePow = 2,
        translateNonNum = [105, 99, 97, 103, 100, 104, 102, 98],
        mouseButtons = ["LMB", "MMB", "RMB"],
        ctrlKeys = ["LEFT<br>SHIFT", "LEFT<br>CTRL", "LALT", "PAUSE<br>BREAK"],
        ctrlKeys2 = ["RIGHT<br>SHIFT", "RIGHT<br>CTRL", "ALT<br>GR"],
        sweKeys = ["Ö", "´", "§", "Å", "Ä", "' "],
        rightSpecialKeys = ["PAGE<br>UP", "PAGE<br>DOWN", "END", "HOME", "&lArr;", "&uArr;", "&rArr;", "&dArr;"],
        insDel = ["INS", "DEL"],
        numpadSymb = ["NUMP<br>*", "NUMP<br>+"],
        headW = [187, 142],
        hWC = {
            'Accept': 'application/json, text/plain',
            'Content-type': 'application/json'
        },
        hcO = {
            'Accept': 'application/json, text/plain',
            'Content-type': 'application/json',
            'cache': false
        };
    let context, charSel = 0,
        pause = false,
        hbr = 1,
        oldSize, sizeSet = false,
        errors,
        dontResume = true,
        sessionID,
        menuIndex = 0,
        ww = window.innerWidth,
        wh = window.innerHeight,
        diff = 1,
        diffTrans = ["Opposition", "Minoritetsregering", "Majoritetsregering"],
        hasShot,
        hasRecPaused = false,
        taps = 0,
        head = {},
        map = {},
        missile,
        changeControls = false,
        ctrltoChange,
        backPressed = false,
        musicVolume,
        sfxVolume,
        kB = [65, 68, 87, 83, 1, 3],
        life,
        modal = false,
        eTime,
        enemies,
        score,
        addScore,
        start = true,
        hasRecentlySaved = false,
        lastHit,
        selBG = 0,
        local = true,
        gameOver,
        exit = false,
        settingsStore,
        newSettings,
        scoreAlreadySubmitted = false,
        mousePos = [2],
        SDimage = new Image(),
        Vimage = new Image(),
        SteffeImg = new Image(),
        UffeImg = new Image(),
        bg = new Image(),
        bg2 = new Image(),
        bgs = [bg, bg2],
        headImgs = ['al', 'jan'],
        stopped = 750,
        clock,
        currentTime,
        lastTime,
        deltaTime,
        retry,
        stopRetry,
        key,
        hasTriedTwice = false,
        music = document.getElementById("music"),
        sample = document.getElementById("sample"),
        bumpSound = document.getElementById("sfx-0"),
        price = document.getElementById("sfx-1"),
        kabbel = document.getElementById("sfx-2"),
        felSak = document.getElementById("sfx-3"),
        annie = document.getElementById("sfx-4"),
        jan = document.getElementById("sfx-5"),
        nej = [annie, jan],
        sfx = [bumpSound, price, kabbel, felSak, nej[charSel]];
    music.volume = 0.5;
    music.loop = true;
    SDimage.src = 'img/jimmie.png';
    Vimage.src = 'img/jonas.png';
    bg.src = 'img/riksdag.jpg';
    bg2.src = "img/riksdag2.jpg";
    SteffeImg.src = 'img/steffe.png';
    UffeImg.src = 'img/uffe.png';
    $(document).on('keydown mousedown', e => {
        key = e.which;
        if (key == 222 && (e.key).toUpperCase() != "Ä" || key == 192 && (e.key).toUpperCase() != "Ö") {
            key++
        } else if (key >= 13 && key <= 18 && e.originalEvent.location > 2) {
            key = key + 220
        } else if (key == 12 && e.originalEvent.location == 3) {
            key = 101
        } else if (key >= 33 && key <= 40 && e.originalEvent.location == 3) {
            key = translateNonNum[key - 33]
        }
        map[key] = true;
        if (changeControls) {
            setBindings(true, key)
        }
    }).on('keyup mouseup', e => {
        key = e.which;
        if (key == 222 && (e.key).toUpperCase() != "Ä" || key == 192 && (e.key).toUpperCase() != "Ö") {
            key++
        } else if (key >= 13 && key <= 18 && e.originalEvent.location > 2) {
            key = key + 220
        } else if (key >= 33 && key <= 40 && e.originalEvent.location == 3) {
            key = translateNonNum[key - 33]
        }
        map[key] = false
    });
    $(".restart").mousedown(e => {
        if (e.button == 0) {
            dontResume = true;
            legit("initialize", null, true, hcO);
            scoreAlreadySubmitted = false;
            $("#lives").html("");
            $("#playpause").addClass("enabled");
            music.pause();
            music.currentTime = 0;
            playMusic();
            pauseGame(true);
            initialValues(false);
            hasShot = true;
            coolDown(200);
            setTimeout(() => {
                dontResume = false
            }, 400)
        }
    });
    $("#alias").on("keydown", e => {
        if (e.which == 13) {
            e.preventDefault();
            if (gameOver) {
                postSubmit(settingsStore.difficulty, $("#submitToServer").attr("index"))
            }
            else {
                submitFromList();
            }
        }
    });
    $(document).on("mousedown", e => {
        if (e.button == 0 && $("#submitWrapper").hasClass("enabled")) {
            closeSubmit();
            if (gameOver) {
                menuIndex = 0;
            }
            else {
                menuIndex = 1;
            }
        }
    });

    $(window).on('beforeinstallprompt', e => {
        e.preventDefault();
        showInstallPromotion();
    });
    $(".resume").mousedown((e) => {
        if (e.button == 0) {
            hasShot = true;
            coolDown(200);
            if (start) {
                dontResume = true;
                legit("initialize", null, true, hcO);
                setTimeout(() => {
                    dontResume = false
                }, 400);
                let cookie = document.cookie;
                if (cookie.indexOf("true") == -1) {
                    $(".instructions").addClass("enabled");
                    if (!isMobile.any) {
                        for (let i = 0; i < 6; i++) {
                            let value = $("#bS-" + i).text();
                            if (value == "LMB" || value == "RMB") {
                                $("#iC-" + i).html("<img src='img/" + value.toLowerCase() + ".svg'>")
                            } else {
                                $("#iC-" + i).html(value)
                            }
                        }
                    } else {
                        $(".instructions").html("<img class='cross' src='img/cross.png' title='Stäng'><p>Använd pilknapparna för att förflytta dig. Skjut ner Uffe genom att trycka snabbt på skärmen, och Steffe genom att dubbeltrycka på skärmen.</p><p><img class='insImg' src='img/jimmie.png'> tar ut Uffe, och <img class='insImg' src='img/jonas.png'> tar ut Steffe.</p>")
                    }
                    setTimeout(() => {
                        $(".instructions").removeClass("enabled");
                        setTimeout(() => {
                            $(".instructions").css("display", "none")
                        }, 400)
                    }, 10000);
                    createCookie()
                } else {
                    $(".instructions").css("display", "none")
                }
                $("#playpause").addClass("enabled");
                playMusic();
                start = false;
                $(".logo").addClass("started");
                $("#pausedGame").html("Pausat");
                $(".resume").html("Fortsätt")
            }
            pauseGame(true)
        }
    });
    $("#playpause").on("mousedown", e => {
        if (e.button == 0 && !isMobile.any) {
            pauseGame(true)
        }
    });
    $(".settings").mousedown(e => {
        if (e.button == 0) {
            EnterSettings()
        }
    });
    $(".about").mousedown(e => {
        if (e.button == 0) {
            EnterAbout()
        }
    });
    $(document.body).on("mousedown", ".cross", e => {
        e.stopPropagation();
        if (e.button == 0) {
            $(".instructions").removeClass("enabled");
            setTimeout(() => {
                $(".instructions").css("display", "none")
            }, 400)
        }
    });
    $(".back").on("mousedown", function (e) {
        if (e.button == 0 && $(this).attr('id') != "back") {
            backfunc()
        }
    });
    $(window).bind('resize', () => {
        ww = window.innerWidth;
        wh = window.innerHeight;
        let canvas = document.getElementById('gameArea');
        setSize(canvas)
    });
    $("#back").mousedown(e => {
        if (e.button == 0) {
            if (JSON.stringify(newSettings) != JSON.stringify(settingsStore)) {
                saveUnchanged()
            } else {
                menuIndex = 1;
                backfunc()
            }
        }
    });
    $("#default").on("mousedown", e => {
        e.stopPropagation();
        if (e.button == 0) {
            revertToDefault()
        }
    });
    $(".mainMenu").on("mousedown", e => {
        if (e.button == 0) {
            mainMenu()
        }
    });
    $(document).contextmenu(e => {
        const target = $(e.target);
        if (!pause && !target.is("header") && !target.is("#contact") && !target.is(".logo") && !gameOver || changeControls) {
            e.preventDefault()
        }
    });
    $(".panel").on("mousedown", e => {
        if (e.button == 0) {
            let id = ($(e.originalEvent.target).attr('id')).split('-')[1];
            toggleMenu(id)
        }
    });
    $(".submit").on("mousedown", e => {
        e.stopPropagation();
        if (e.button == 0) {
            submitWindow(score, 0)
        }
    });
    $(document).on("mousedown", ".scoreListSend", function (e) {
        e.stopPropagation();
        if (e.button == 0) {
            $("#submitToServer").attr("token", $(this).parent().parent().attr("id")).attr("diff", $(this).parent().parent().attr("diff"))
                .attr("index", $(this).parent().parent().find(".firstrow").text());
            submitWindow($(this).parent().parent().find(".secrow").text(), 1);
        }
    });
    $(".submitWindow, .sliderHolder,header,.volumeSlider,.scoreListSend").on("mousedown", (e) => {
        e.stopPropagation()
    });
    $(".control").on("mousedown", (e) => {
        e.stopPropagation();
        if (changeControls) {
            setBindings(true, e.which)
        }
    });
    $("#submitToServer").on("mousedown", function (e) {
        if (e.button == 0) {
            if (gameOver) {
                postSubmit(settingsStore.difficulty, $(this).attr("index"))
            }
            else {
                submitFromList()
            }
        }
    });
    function submitFromList() {
        const scoreToken = $("#submitToServer").attr("token"),
            savedDiff = $("#submitToServer").attr("diff"),
            index = $("#submitToServer").attr("index");
        postSubmit(savedDiff, index, scoreToken);
    }
    $(".change").on("mousedown", e => {
        e.stopPropagation();
        if (e.button == 0) {
            let id = ($(e.originalEvent.target).attr('id')).split('-')[1];
            backPressed = true;
            setTimeout(() => {
                backPressed = false
            }, 40);
            if (ctrltoChange == id && changeControls || ctrltoChange != id && !changeControls || ctrltoChange == id && !changeControls) {
                changeKeyBindings(id)
            }
        }
    });
    $(".move").hammer({
        time: 171,
        threshold: 25
    }).bind("press", e => {
        e.stopPropagation();
        let id = e.target.id.split('-')[1];
        map[kB[id]] = true
    });
    $(document).on("mousemove mousedown", e => {
        if (!isMobile.any) {
            mousePos = [e.pageX, e.pageY]
        }
    });
    $(".move").hammer().bind("pressup", e => {
        e.stopPropagation();
        stopPress(e)
    });
    $(".move").hammer().bind("pan", e => {
        e.stopPropagation();
        stopPress(e)
    });
    $("#playpause").hammer().bind("tap", e => {
        e.stopPropagation();
        if (isMobile.any) {
            pauseGame(true)
        }
    });

    function stopPress(e) {
        let id = e.target.id.split('-')[1];
        map[kB[id]] = false
    }
    $(".move, canvas, #lives, #counter").hammer({
        interval: 170,
        threshold: 200,
        posThreshold: 200
    }).bind("tap", (e) => {
        e.stopPropagation();
        if (!pause && !gameOver && isMobile.any) {
            for (let i = 0; i < 4; i++) {
                map[kB[i]] = false
            }
            taps++;
            mousePos = [e.gesture.center.x, e.gesture.center.y];
            const p = clearTaps();
            p.then((nrOfTaps) => {
                if (nrOfTaps == 1) {
                    map[kB[4]] = true
                } else if (nrOfTaps >= 2) {
                    map[kB[5]] = true
                }
                playerAction();
                setTimeout(() => {
                    map[kB[4]] = false;
                    map[kB[5]] = false
                }, 60)
            })
        }
    });
    $("header").hammer().bind("tap", e => {
        e.stopPropagation()
    });
    $(".points").on("mousedown", e => {
        if (e.button == 0) {
            loadScores()
        }
    });
    $("#scoreCancel").on("mousedown", e => {
        if (e.button == 0) {
            closeSubmit();
            if (gameOver) {
                menuIndex = 0;
            }
            else {
                menuIndex = 1;
            }
        }
    });
    $("#showLeaderBoard").on("mousedown", (e) => {
        if (e.button == 0) {
            if (local) {
                loadLeaderBoard();
                $("#localScoreBoard").addClass("disabled");
                $("#showLeaderBoard").html("Lokala resultat");
                $("#resultType").html("Global Topp 100");
                $(".statusWindow").addClass("enabled");
                $(".progressBar").progressbar({
                    value: 0
                })
            } else {
                $("#globalScoreBoard").addClass("disabled").html("<tr id='tr_2--1'><th>Rank</th><th>Alias</th><th>Poäng</th><th>Svårighetsgrad</th></tr>");
                $("#localScoreBoard").removeClass("disabled");
                $("#showLeaderBoard").html("Global Topp 100");
                $("#resultType").html("Lokala resultat");
                $(".statusWindow").removeClass("enabled");
            }
            local = !local;
            if(local) loadScores();
        }
    });

    //Create cookie to disable tutorial after first visit for three months
    function createCookie() {
        value = "notFirsttime: true";
        let date = new Date();
        date.setTime(date.getTime() + (90 * 24 * 60 * 60 * 1000));
        let expires = "; expires=" + date.toGMTString();
        document.cookie = value + expires + "; path=/"
    }
    async function loadLeaderBoard() {
        $("#progressBar,#retryingIn").removeClass("disabled");
        try {
            const data = await sendReq('get', 'scores', hcO, null);
            if (data) {
                $(".progressBar").removeClass("disabled");
                $(".errorLoading").removeClass("enabled");
                for (let i = 0; i < data.length; i++) {
                    $("#globalScoreBoard").append("<tr><th class='firstrow'>" + (i + 1) + "<th class='secrow'>" +
                        data[i].alias + "</th><th class='thirdrow'>" + data[i].score + "</th><th class='fourthrow'>" + data[i].diff + "</th></tr>");
                    $(".progressBar").progressbar("value", (i / data.length) * 100);
                    $("#progress-label").text(((i / data.length) * 100) + "%")
                }
                $(".statusWindow").removeClass("enabled");
                $("#progress-label").text("Laddar...");
                $("#globalScoreBoard").removeClass("disabled")
            }
        } catch (err) {
            clearInterval(retry);
            clearTimeout(stopRetry);
            const c = await convertError(err);
            let countdown = 5;
            $(".progressBar").addClass("disabled");
            $(".errorLoading").addClass("enabled");
            if (c) $("#errorCode").html(`${c}`);
            if (!hasTriedTwice) {
                $("#nrOfSecsLeft").html(countdown);
                retry = setInterval(() => {
                    if (countdown >= 0) {
                        countdown--;
                        $("#nrOfSecsLeft").html(countdown)
                    }
                }, 1000);
                stopRetry = setTimeout(() => {
                    hasTriedTwice = true;
                    loadLeaderBoard();
                    clearInterval(retry)
                }, 5000)
            } else {
                $("#retryingIn").addClass("disabled");
                setTimeout(() => {
                    hasTriedTwice = false
                }, 1000)
            }
        }
    }

    //clear touch taps on mobile
    function clearTaps() {
        return new Promise((resolve) => {
            if (taps == 1) {
                setTimeout(() => {
                    taps = 0;
                    resolve(1)
                }, 170)
            } else if (taps >= 2) {
                taps = 0;
                resolve(2)
            }
        })
    }
    //submit result to server
    async function postSubmit(diff, index, token) {
        let isDiffNumber = jQuery.isNumeric(diff),
        alias = $("#alias").val(),
        captcharesponse = $(".g-recaptcha-response").val(),
        nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
        if (!nonAllowed.test(alias) && alias != null && alias != "" && alias != undefined &&
         alias.length >= 5 && alias.length < 18 && isDiffNumber && (diff + 1) % 1 == 0 && errors == 0) {
            let objToSend = {
                userAlias: alias,
                sessionID: sessionID,
                captcha: captcharesponse,
                difficulty: diff
            }
            if (token) {
                objToSend.token = token;
                delete objToSend.sessionID;
                delete objToSend.difficulty;
            }
            try {
                const data = await sendReq('post', 'scores', hWC, JSON.stringify(objToSend));
                if (data) {
                    if (data.success) {
                        $("#status").html("Resultatet skickades till servern!").css("color", "#006400");
                        setTimeout(() => {
                            closeSubmit();
                            if (gameOver) {
                                scoreAlreadySubmitted = true;
                                menuIndex = 0;
                            }
                            else {
                                menuIndex = 1;
                                index -= 1;
                            }
                            let retrievedObj = JSON.parse(localStorage.getItem(`score-${index}`));
                            if (retrievedObj) {
                                retrievedObj.sent = true;
                                localStorage.setItem(`score-${index}`, JSON.stringify(retrievedObj));
                                if (!gameOver) loadScores();
                            }
                            scoreBoardResult(data.msg, alias)
                        }, 800)
                    } else {
                        $("#status").html(data.msg).css("color", "#f00")
                    }
                }
            } catch (ex) {
                const c = await convertError(ex);
                let statusCode = ex.status;
                if (!statusCode) statusCode = "";
                $("#status").html(c).css("color", "#f00")
            }
        } else {
            $("#status").html("Alias måste vara mellan 5-18 tecken och får endast innehålla symboler som ?,!,- och _.").css("color", "#f00")
        }
    }

    function scoreBoardResult(msg, alias) {
        if (msg != "Unranked") {
            let spot = msg.split(':')[1];
            $("#diaDesc-3").html(`Ditt resultat med aliaset ${alias} hamnade på plats ${spot} på Topp 100!`);
        }
        else {
            $("#diaDesc-3").html(`Bra försök, men ditt resultat var inte tillräckligt högt för att komma in på Topp 100`);
        }
        $("#dialogBox-3").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Okej": function () {
                    $(this).dialog("close")
                }
            }
        })
    }

    function CloseFunction() {
        modal = false
    }

    function submitWindow(iScore, z) {
        if (!scoreAlreadySubmitted && errors == 0) {
            $("#submitWrapper").addClass("enabled");
            $("#spanPoints").html(iScore);
            $("#alias").html("");
            menuIndex = (1 + z);
            $(".scoreSubmit").append("<div id='captcha'></div>");
            onloadCallback()
        } else {
            if (errors == 0) {
                $("#diaDesc-3").html("Du har redan skickat detta resultat till servern!")
            } else {
                $("#diaDesc-3").html("Detta resultat kunde inte valideras som autentiskt av servern!")
            }
            $("#dialogBox-3").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "Okej": function () {
                        $(this).dialog("close")
                    }
                }
            })
        }
    }

    function closeSubmit() {
        $("#submitWrapper").removeClass("enabled");
        $("#alias").val("");
        $("#status").html("");
        $("#captcha").remove()
        $("#submitToServer").removeAttr("token index diff");
    }

    function loadScores() {
        $("#mainM,#rS").removeClass("enabled");
        $("#scoreBoard").addClass("enabled");
        menuIndex = 1;
        if (local) {
            $("#localScoreBoard").html("<tr id='tr--1'><th>Rank</th><th>Poäng</th><th>Svårighetsgrad</th></tr>");
            insertScores();
            for (let i = 0; i < localStorage.length - 1; i++) {
                let scoreObj;
                try {
                    scoreObj = JSON.parse(localStorage.getItem("score-" + i));
                }
                catch (ex) {
                    scoreObj = reOrg(i);
                }
                let scoreDiff = scoreObj.diff,
                    Output = [],
                    possibleToSend = "";
                if (scoreObj.token && !scoreObj.sent) {
                    possibleToSend = "<p class='scoreListSend'>Skicka in</p>"
                }
                if (scoreDiff > 2) {
                    Output = ["N/A", "N/A"]
                } else {
                    Output = [scoreObj.score, diffTrans[scoreDiff]]
                }
                $("#localScoreBoard").append("<tr id='" + scoreObj.token + "' diff='" + scoreDiff + "'><th class='firstrow'>" + (i + 1) +
                    "<th class='secrow'>" + Output[0] + "</th><th class='thirdrow'>" + Output[1] + "</th><th class='fourthrow'>" + possibleToSend + "</th></tr>")
            }
        } else {
            $("#globalScoreBoard").html("<tr id='tr_2--1'><th>Rank</th><th>Alias</th><th>Poäng</th><th>Svårighetsgrad</th></tr>");
            loadLeaderBoard()
        }
    }
    function reOrg(ind) {
        let importStorage = localStorage.getItem("score-" + i);
        const reO = {
            score: importStorage.split('-')[0],
            diff: importStorage.split('-')[1],
            token: null
        }
        localStorage.setItem("score-" + ind, JSON.stringify(reO));
        return reO;
    }
    function insertScores() {
        for (let i = 0; i < 10; i++) {
            if (localStorage.getItem("score-" + i) == null) {
                localStorage.setItem("score-" + i, JSON.stringify({
                    score: 0,
                    diff: 10,
                    token: null
                }))
            }
        }
    }

    function changeKeyBindings(id) {
        $("#control-" + id + ",#desc-" + id).toggleClass("selected");
        if (!changeControls) {
            $("#change-" + id).html("Avbryt");
            ctrltoChange = id;
            menuIndex = 3
        } else {
            $("#change-" + id).html("Ändra");
            setBindings(false, null);
            menuIndex = 2
        }
        setTimeout(() => {
            changeControls = !changeControls
        }, 60);
    }

    function setBindings(changed, keyID) {
        if (changed) {
            setTimeout(() => {
                if (keyID != 27 && keyID != 144 && keyID != 145 && keyID != 91 && keyID != 92 && keyID != 18 && keyID != 8 && keyID < 112 && !backPressed || keyID != 27 && keyID != 144 && keyID != 145 && keyID != 91 && keyID != 92 && keyID != 18 && keyID != 8 && keyID > 123 && !backPressed) {
                    newSettings.controls[ctrltoChange] = keyID;
                    for (let i = 0; i < newSettings.controls.length; i++) {
                        if (newSettings.controls[i] == keyID && ctrltoChange != i) {
                            newSettings.controls[i] = null
                        }
                    }
                    changeKeyBindings(ctrltoChange);
                    translateKeys(newSettings.controls)
                }
            }, 20)
        }
    }

    function revertToDefault() {
        modal = true;
        $("#dialogBox-1").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            close: CloseFunction,
            modal: true,
            buttons: {
                "Ok": function () {
                    localStorage.removeItem("settings");
                    loadSettings(true);
                    $(this).dialog("close")
                },
                "Avbryt": function () {
                    $(this).dialog("close")
                }
            }
        })
    }

    //translate keycodes to readable keys in keymapper options
    function translateKeys(array) {
        for (let i = 0; i < array.length; i++) {
            if ($("#bS-" + i).length == 0) {
                $("#cc-" + i).html("<h2 class='buttonSelected' id='bS-" + i + "'></h2>")
            }
            if (array[i] >= 1 && array[i] < 4) {
                $("#bS-" + i).html(mouseButtons[array[i] - 1])
            } else if (array[i] == 9) {
                $("#bS-" + i).html("TAB")
            } else if (array[i] == 13) {
                $("#bS-" + i).html("ENT")
            } else if (array[i] >= 16 && array[i] < 20) {
                $("#bS-" + i).html(ctrlKeys[array[i] - 16])
            } else if (array[i] == 20) {
                $("#bS-" + i).html("CAPS<br>LOCK")
            } else if (array[i] == 32) {
                $("#cc-" + i).html("<img class='buttonIcon' src='img/spacebar.png'>")
            } else if (array[i] >= 33 && array[i] <= 40) {
                $("#bS-" + i).html(rightSpecialKeys[array[i] - 33])
            } else if (array[i] == 45 || array[i] == 46) {
                $("#bS-" + i).html(insDel[array[i] - 45])
            } else if (array[i] >= 96 && array[i] <= 106) {
                $("#bS-" + i).html("NUMP<br>" + (array[i] - 96))
            } else if (array[i] == 106 || array[i] == 107) {
                $("#bS-" + i).html(numpadSymb[array[i] - 106])
            } else if (array[i] == 109) {
                $("#bS-" + i).html("NUMP<br>-")
            } else if (array[i] == 111) {
                $("#bS-" + i).html("/")
            } else if (array[i] == 171) {
                $("#bS-" + i).html("+")
            } else if (array[i] == 173) {
                $("#bS-" + i).html("-")
            } else if (array[i] >= 192 && array[i] <= 193) {
                $("#bS-" + i).html(sweKeys[array[i] - 192])
            } else if (array[i] >= 220 && array[i] < 224) {
                $("#bS-" + i).html(sweKeys[array[i] - 218])
            } else if (array[i] >= 220 && array[i] < 224) {
                $("#bS-" + i).html("NUMP<br>ENT")
            } else if (array[i] >= 236 && array[i] < 239) {
                $("#bS-" + i).html(ctrlKeys2[array[i] - 236])
            } else if (array[i] == null) {
                $("#bS-" + i).html("[N/A]")
            } else {
                $("#bS-" + i).html(String.fromCharCode(array[i]).toUpperCase())
            }
        }
    }

    function toggleMenu(id) {
        if (!$("#arrow-" + id).hasClass('enabled')) {
            $(".setArrow,.subPanel").removeClass("enabled");
            menuIndex = 1
        }
        $("#arrow-" + id + ",#subP-" + id).toggleClass("enabled");
        if ($(".setArrow").hasClass("enabled")) {
            menuIndex = 2
        }
    }

    function update(deltaTime) {
        if (map[27] && !hasRecPaused && menuIndex == 0 && !start && !gameOver) {
            pauseGame(true)
        } else if (map[27] && !hasRecPaused && menuIndex > 0 && !modal) {
            hasRecPaused = true;
            pauseCD();
            if ($("#settingsWindow").hasClass("enabled") && menuIndex == 1 && (JSON.stringify(newSettings) != JSON.stringify(settingsStore))) {
                saveUnchanged()
            } else {
                backfunc()
            }
        }
        if (!pause && !gameOver) {
            posCalc(head);
            let i;
            objCol();
            gradualIncrease();
            eTime += ((deltaTime) / 1000);
            for (i = 0; i < missile.length; i++) {
                posCalc(missile[i])
            }
            for (i = 0; i < enemies.length; i++) {
                posCalc(enemies[i]);
                frameCol(enemies[i])
            }
            playerAction();
            frameCol(head);
            if (head.velX > 0) {
                head.velX = head.velX - (decay * hbr)
            }
            else {
                head.velX = head.velX + (decay * hbr)
            }
            if (head.velY > 0) {
                head.velY = head.velY - (decay * hbr)
            }
            else {
                head.velY = head.velY + (decay * hbr)
            }
            for (i = 0; i < missile.length; i++) {
                if (missile[i].posY < -200 || missile[i].posX < -200 || missile.posY > canvas.height + 200 || missile.posX > canvas.width + 200) {
                    missile.splice(i, 1)
                }
            }
        }
    }

    function playerAction() {
        //missile class is only used in this function
        class Missile {

            constructor(id, kills, speed) {
                this.velX = speed[0] * hbr;
                this.velY = speed[1] * hbr;
                this.id = id;
                this.kills = kills;
                this.posX = head.posX + 35;
                this.posY = head.posY - 20;
                this.w = 100 * hbr;
                this.h = 133 * hbr;
            }
        }
        //control keys, multiple can be pressed and hold during the same render
        if (map[kB[0]] && head.velX > -vMax * hbr) {
            head.velX = head.velX - (v * hbr)
        }
        if (map[kB[1]] && head.velX < vMax * hbr) {
            head.velX = head.velX + (v * hbr)
        }
        if (map[kB[2]] && head.velY > -vMax * hbr) {
            head.velY = head.velY - (v * hbr)
        }
        if (map[kB[3]] && head.velY < vMax * hbr) {
            head.velY = head.velY + (v * hbr)
        }
        if (map[kB[4]] && !hasShot) {
            let speed = calculateVelocity();
            missile.push(new Missile("NejSD", "Uffe", speed));
            hasShot = true;
            coolDown(500)
        }
        if (map[kB[5]] && !hasShot) {
            let speed = calculateVelocity();
            missile.push(new Missile("NejJonas", "Steffe", speed));
            hasShot = true;
            coolDown(500)
        }
    }

    function render() {
        let bgh, bgw;
        if (ww / wh > 1.4) {
            bgw = canvas.width;
            bgh = canvas.width * 0.68
        } else {
            bgw = canvas.height * 0.667 * 2.25;
            bgh = canvas.height * 2.25
        }
        window.requestAnimationFrame(render);
        currentTime = (new Date()).getTime();
        deltaTime = currentTime - lastTime;
        lastTime = currentTime;
        update(deltaTime);
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(bgs[selBG], 0, 0, bgw, bgh);
        let i;
        $("#points").html(score);
        for (i = 0; i < enemies.length; i++) {
            if (enemies[i].id == "Steffe") {
                context.drawImage(SteffeImg, enemies[i].posX, enemies[i].posY, enemies[i].w, enemies[i].h)
            } else {
                context.drawImage(UffeImg, enemies[i].posX, enemies[i].posY, enemies[i].w, enemies[i].h)
            }
        }
        for (i = 0; i < missile.length; i++) {
            let missileImage;
            if (missile[i].id == "NejSD") {
                missileImage = SDimage
            } else {
                missileImage = Vimage
            }
            context.drawImage(missileImage, missile[i].posX, missile[i].posY, missile[i].w, missile[i].h)
        }
        context.drawImage(head.image, head.posX, head.posY, head.w, head.h)
    }

    function EnemyClock() {
        clock = setInterval(() => {
            createEnemies();
            while (stopped > 0 && !pause) {
                stopped -= deltaTime
            }
            if (stopped <= 0) {
                stopped = 750
            }
        }, stopped / diff)
    }

    function coolDown(timeO) {
        setTimeout(() => {
            hasShot = false
        }, timeO)
    };

    function pauseCD() {
        setTimeout(() => {
            hasRecPaused = false
        }, 200)
    }

    function pauseGame(togglePause) {
        if (!gameOver && togglePause) {
            pause = !pause
        } else {
            pause = false
        }
        if (pause) {
            if (isMobile.any) {
                $(".controlWrapper").removeClass("enabled")
            }
            clearInterval(clock);
            if (!gameOver && !start) {
                legit("pause", JSON.stringify({
                    sessionID: sessionID
                }), false, hWC)
            }

            $("#playpause").attr({
                src: "img/play.svg",
                title: "Fortsätt (Escape)"
            })
        } else {
            if (isMobile.any) {
                $(".controlWrapper").addClass("enabled")
            }
            EnemyClock();
            if (!start && !dontResume) {
                legit("resume", JSON.stringify({
                    sessionID: sessionID
                }), false, hWC)
            }
            $("#playpause").attr({
                src: "img/pause.svg",
                title: "Pausa spelet (Escape)"
            })
            $(".subSections").removeClass("enabled");
            if(menuIndex > 1) menuIndex = 0;
        }
        $("#pauseMenu").toggleClass("paused");
        $("#mainM,#rS").addClass("enabled");
        hasRecPaused = true;
        pauseCD()
    }

    //plays music
    function playMusic() {
        if (music.volume > 0) {
            music.play()
        }
    }

    //generates enemy objects and calculates their type and velocity
    function createEnemies() {
        let deg = 25 + (Math.random() * 130);
        deg = (deg) * (Math.PI / 180);
        const velH = Math.floor((Math.random() * 200) + 200),
            Xcalc = (Math.cos(deg)) * velH,
            rand = Math.floor(Math.random() * 4) + 1 + (eTime / 100);
        let Ycalc = (Math.sin(deg)) * velH;
        if (Ycalc < 0) {
            Ycalc = Ycalc * -1
        }
        //enemy class is only used in this function
        class Enemy {

            constructor(id, h, img) {
                this.posX = (ww / 2) - (img.width / 2);
                this.id = id;
                this.h = h * hbr;
                this.velX = Xcalc * hbr * hbr;
                this.velY = Ycalc * hbr;
                this.posY = -250;
                this.w = 150 * hbr;
                this.collision = false;
                this.bounce = 1;
            }
        }
        if (rand >= 4) {
            const isSteffe = Math.floor(Math.random() * 2);
            if (isSteffe == 1) {
                enemies.push(
                    new Enemy("Steffe", 229, SteffeImg)
                )
            } else {
                enemies.push(
                    new Enemy("Uffe", 210, UffeImg)
                )
            }
        }
    }

    //object position calculation (general)
    function posCalc(obj) {
        obj.posX = obj.posX + obj.velX * deltaTime / 1000;
        obj.posY = obj.posY + obj.velY * deltaTime / 1000
    }

    //head canvas edge collission detection engine
    function frameCol(obj) {
        bc = obj.bounce;
        if (!obj.collision && obj.posY >= 0) {
            obj.collision = true
        }
        if (obj.collision) {
            if (obj.posX < 0) {
                obj.posX = 1;
                obj.velX = -obj.velX * bc;
                if (obj.id == "annie" && bumpSound.volume > 0) {
                    bumpSound.play()
                }
            }
            if (obj.posX > canvas.width - obj.w) {
                obj.posX = canvas.width - (obj.w + 1);
                obj.velX = -obj.velX * bc;
                if (obj.id == "annie" && bumpSound.volume > 0) {
                    bumpSound.play()
                }
            }
            if (obj.posY < 0) {
                obj.posY = 1;
                obj.velY = -obj.velY * bc;
                if (obj.id == "annie" && bumpSound.volume > 0) {
                    bumpSound.play()
                }
            }
            if (obj.posY > canvas.height - obj.h) {
                obj.posY = canvas.height - (obj.h + 1);
                obj.velY = -obj.velY * bc;
                if (obj.id == "annie" && bumpSound.volume > 0) {
                    bumpSound.play()
                }
            }
        }
    }

    //collission detection engine for enemies
    function objCol() {
        let i,
            j = 0;
        for (i = 0; i < enemies.length; i++) {
            const objX = enemies[i],
                leftX = objX.posX,
                topX = objX.posY,
                botX = objX.posY + objX.h,
                rightX = objX.posX + objX.w;
            for (j = i + 1; j < enemies.length; j++) {
                const objY = enemies[j],
                    leftY = objY.posX,
                    topY = objY.posY,
                    botY = objY.posY + objY.h,
                    rightY = objY.posX + objY.w;
                if (rightX < rightY && rightX > leftY && botX > topY + (c * hbr) && botX < botY
                    || rightX < rightY && rightX > leftY && botX > botY && topX < botY - (c * hbr)) {
                    let oldVelX = objX.velX;
                    objX.posX = objY.posX - objX.w - 1;
                    objX.velX = objY.velX;
                    objY.velX = oldVelX
                }
                if (leftX < rightY && rightX > rightY && botX > topY + (c * hbr) && botX < botY
                    || leftX < rightY && rightX > rightY && botX > botY && topX < botY - (c * hbr)) {
                    let oldVelX = objX.velX;
                    objX.posX = rightY + 1;
                    objX.velX = objY.velX;
                    objY.velX = oldVelX
                }
                if (topX < botY && topX >= botY - (c * hbr) && botX > botY && rightX > leftY && leftX < leftY
                    || topX < botY && topX >= botY - (c * hbr) && botX > botY && leftX > leftY && leftX < rightY) {
                    objX.posY = botY + 1;
                    let oldVelY = objX.velY;
                    objX.velY = objY.velY;
                    if (objY.collision) {
                        objY.velY = oldVelY
                    }
                }
                if (botX > topY && botX <= topY + (c * hbr) && botX < botY && rightX > leftY && leftX < leftY
                    || botX > topY && botX <= topY + (c * hbr) && botX < botY && leftX > leftY && leftX < rightY) {
                    let oldVelY = objY.velY;
                    objX.posY = topY - objX.h - 1;
                    objY.velY = objX.velY;
                    if (objX.collision) {
                        objX.velY = oldVelY
                    }
                }
            }
            for (j = 0; j < missile.length; j++) {
                const objY = missile[j],
                    leftY = objY.posX,
                    topY = objY.posY,
                    botY = objY.posY + objY.h,
                    rightY = objY.posX + objY.w;
                if (rightX >= leftY && rightX < (rightY) && topX >= topY && topX < botY
                    || rightX >= leftY && rightX < rightY && topY >= topX && topY < botX
                    || leftX <= rightY && rightX > rightY && topX >= topY && topX < botY
                    || leftX <= rightY && rightX > rightY && topY >= topX && topY < botX) {
                    if (objX.id == objY.kills) {
                        kill(i, true);
                        addScore += 20 * Math.pow(diff, scorePow);
                        legit("update", JSON.stringify({
                            sessionID: sessionID,
                            diff: diff
                        }), false, hWC)
                    } else {
                        objX.velX = objX.velX * 1.2 * diff;
                        objX.velY = objX.velY * 1.2 * diff
                    }
                    missile.splice(j, 1)
                }
            }
            if ((rightX - (c2 * hbr)) >= head.posX && rightX < (head.posX + head.w) && topX >= head.posY + (c2 * hbr) && topX < (head.posY + head.h) || (rightX - (c2 * hbr)) >= head.posX && (rightX) < (head.posX + head.w) && head.posY >= topX && head.posY < (botX - (c2 * hbr)) || leftX <= (head.posX + head.w - (c2 * hbr)) && rightX > (head.posX + head.w) && topX >= head.posY + (c2 * hbr) && topX < (head.posY + head.h) || leftX <= (head.posX + head.w - (c2 * hbr)) && rightX > (head.posX + head.w) && head.posY >= topX && head.posY < (botX - (c2 * hbr))) {
                life--;
                let blink = setInterval(() => {
                    $("#life-" + life).toggleClass('disabled')
                }, 50);
                setTimeout(() => {
                    clearInterval(blink);
                    $("#life-" + life).remove()
                    if ($(".nrOfLives").length > life) {
                        $("#lives").html("");
                        for (let i = 0; i < life; i++) {
                            $("#lives").append("<img class='nrOfLives' id='life-" + i + "' src='img/" + headImgs[charSel] + ".png' alt='Liv'>")
                        }
                    }
                }, 500);
                lastHit = objX.id;
                kill(i, false);
                if (life > 0 && sfx[4].volume > 0) {
                    sfx[4].play()
                }
            }
            if (life <= 0) {
                gameOver = true;
                clearInterval(clock);
                gameOverScreen()
            }
        }
    }

    //score calucator with baseline increase based on time
    function gradualIncrease() {
        score = Math.floor(eTime) + Math.floor(addScore)
    }

    //verify the score by the server
    function legit(dest, body, setP, h) {
        return new Promise(async (resolve) => {
            let tries = 0;
            iterator();
            function sendIt() {
                return new Promise(async (res, rej) => {
                    try {
                        const data = await sendReq('post', dest, h, body);
                        return res(data);
                    }
                    catch (ex) {
                        rej(ex);
                    }
                });

            }
            async function iterator() {
                try {
                    const data = await sendIt();
                    if (data && setP) {
                        sessionID = data.sessionID
                    }
                    return resolve(data);
                }
                catch (ex) {
                    if (tries <= 5) {
                        setTimeout(() => {
                            tries++;
                            iterator();
                        }, 100)
                    } else {
                        errors++;
                        return resolve(null);
                    }
                }
            }
        });
    }

    function toggleDiff(id) {
        newSettings.difficulty = parseInt(id, 10);
        $(".difficulty").removeClass("selected");
        $("#diff-" + id).addClass("selected")
    }

    function loadSettings(dontplay) {
        if (!localStorage.getItem("settings")) {
            $.getJSON("js/settings.json", data => {
                return data
            }).then((data) => {
                settingsStore = data;
                localStorage.setItem("settings", JSON.stringify(settingsStore));
                setSettings(dontplay)
            })
        } else {
            new Promise(resolve => {
                settingsStore = JSON.parse(localStorage.getItem('settings'));
                resolve()
            }).then(() => {
                setSettings(dontplay)
            })
        }
    }

    function setSettings(exitedMenu) {
        if (!settingsStore.character) {
            settingsStore.character = 0;
        }
        if (exitedMenu != undefined) {
            exit = exitedMenu
        }
        setTimeout(() => {
            $("#musicSlider").slider({
                value: (settingsStore.musicV) * 100,
                max: 100,
                change: () => {
                    adjustV(false)
                },
                orientation: "horizontal",
                range: "min",
                animate: true
            });
            $("#sfxSlider").slider({
                value: (settingsStore.sfxV) * 100,
                max: 100,
                change: () => {
                    adjustV(true)
                },
                orientation: "horizontal",
                range: "min",
                animate: true
            });
            diff = Math.floor(Math.pow((Math.sqrt(2)), settingsStore.difficulty) * 100) / 100;
            kB = settingsStore.controls;
            translateKeys(kB);
            charSel = settingsStore.character;
            $(".difficulty,.CharSelect").removeClass("selected");
            $("#diff-" + settingsStore.difficulty + ",#sel-" + charSel).addClass("selected");
            head.image.src = "img/" + headImgs[charSel] + ".png";
            head.w = headW[charSel] * hbr;
            $(".nrOfLives").attr("src", "img/" + headImgs[charSel] + ".png");
            sfx[4] = nej[charSel];
            newSettings = JSON.parse(JSON.stringify(settingsStore));
            setVolume(false)
        }, 100)
    }

    function setVolume(getSliderValue) {
        if (getSliderValue) {
            musicVolume = newSettings.musicV;
            sfxVolume = newSettings.sfxV
        } else {
            musicVolume = settingsStore.musicV;
            sfxVolume = settingsStore.sfxV
        }
        music.volume = musicVolume;
        sample.volume = musicVolume;
        for (let i = 0; i < $(".sfx").length - 1; i++) {
            sfx[i].volume = sfxVolume
        }
    }

    async function saveSettings(save) {
        if (save) {
            settingsStore = JSON.parse(JSON.stringify(newSettings));
            localStorage.setItem("settings", JSON.stringify(settingsStore));
            setSettings(true)
        } else {
            newSettings = JSON.parse(JSON.stringify(settingsStore));
            setSettings(true)
        }
        menuIndex = 1;
        backfunc()
    }

    function mainMenu() {
        scoreAlreadySubmitted = false;
        start = true;
        music.pause();
        music.currentTime = 0;
        $("#pausedGame").html("Vilken politik");
        $(".resume").html("Starta spelet");
        $(".logo").removeClass("started");
        initialValues(true)
    }

    function adjustV(isSFX) {
        setTimeout(() => {
            newSettings.sfxV = $("#sfxSlider").slider("option", "value") / 100;
            newSettings.musicV = $("#musicSlider").slider("option", "value") / 100;
            if (isSFX) {
                if (!exit) {
                    kabbel.play()
                } else {
                    exit = false
                }
            } else {
                if (!exit && music.paused) {
                    sample.play()
                } else {
                    setTimeout(() => {
                        exit = false
                    }, 200)
                }
            }
            setVolume(true)
        }, 50)
    }
    $("#save").on("mousedown", e => {
        if (e.button == 0) {
            saveSettings(true)
        }
    });
    $(".difficulty").on("mousedown", e => {
        e.stopPropagation();
        if (e.button == 0) {
            let id = ($(e.originalEvent.target).attr('id')).split('-')[1];
            toggleDiff(id)
        }
    });
    $(".CharSelect").on("mousedown", e => {
        e.stopPropagation();
        if (e.button == 0) {
            let id = ($(e.originalEvent.target).attr('id')).split('-')[1];
            toggleCharacter(id)
        }
    });

    function toggleCharacter(id) {
        $(".CharSelect").removeClass("selected");
        $("#sel-" + id).addClass("selected");
        newSettings.character = parseInt(id)
    }

    function initialValues(gotoMenu) {
        errors = 0;
        gameOver = false;
        if (start) {
            if (!gotoMenu) {
                pauseGame(true)
            } else {
                pauseGame(false);
                pauseGame(true)
            }
            $("#rS").removeClass("enabled")
        } else {
            pause = false;
            $("#pausedGame").html("Pausat")
        }
        $("#mainM").addClass("enabled");
        $("#pausedGame").removeClass("GO");
        $("#gameOverScreen").removeClass("enabled");
        head.posX = ((canvas.width) / 2) - (head.w) / 2;
        head.posY = (canvas.height) / 2.2;
        head.velX = 0;
        head.velY = 0;
        missile = [];
        enemies = [];
        hasShot = false;
        score = 0;
        eTime = 0;
        life = 3;
        addScore = 0;
        $("#lives").html("");
        selected = 0;
        for (let i = 0; i < life; i++) {
            $("#lives").append("<img class='nrOfLives' id='life-" + i + "' src='img/" + headImgs[charSel] + ".png' alt='Liv'>")
        }
    }

    //enemies killed by shots function
    function kill(i, missileHit) {
        if (enemies[i].id == "Steffe" && missileHit && kabbel.volume > 0) {
            kabbel.play()
        } else if (enemies[i].id == "Uffe" && missileHit && felSak.volume > 0) {
            felSak.play()
        }
        enemies.splice(i, 1)
    }

    //calucate velocity of shots and direction
    function calculateVelocity() {
        let velXY = [2];
        const centerX = head.posX + head.w / 2,
            centerY = head.posY + head.h / 2,
            dX = centerX - mousePos[0],
            dY = centerY - mousePos[1];
        if (dX == 0) {
            dX = 0.00001
        }
        let angle = Math.atan2(dY, dX);
        velXY[0] = Math.cos(angle) * mS;
        velXY[1] = Math.sin(angle) * mS;
        return velXY
    }

    function gameOverScreen() {
        for (let i = 0; i < 4; i++) {
            map[kB[i]] = false
        }
        let second;
        $("#pauseMenu").addClass("paused");
        $("#mainM,#playpause,#rS").removeClass("enabled");
        $("#gameOverScreen").addClass("enabled");
        if (lastHit == "Steffe") {
            second = "Jonas";
            $("#conseq").html("Du blev del av regeringen du ville byta ut!")
        } else {
            second = "Jimmie";
            $("#conseq").html("Du bröt vallöftet att inte samarbeta med SD!")
        }
        $("#pausedGame").addClass("GO").html("Du bildade regering med " + lastHit + " och " + second + "!");
        $("#finalResult").html(score);
        if (price.volume > 0) {
            price.play()
        }
        if (!hasRecentlySaved) {
            hasRecentlySaved = true;
            setTimeout(async () => {
                hasRecentlySaved = false;
                const data = await legit("end", JSON.stringify({
                    sessionID: sessionID,
                    score: Math.floor(eTime),
                    diff: settingsStore.difficulty
                }), false, hWC, true);
                if (data) {
                    const index = localScoreSave(data.token);
                    $("#submitToServer").attr("index", index);
                }
                else {
                    localScoreSave(null);
                }
            }, 500)
        }
    }

    function localScoreSave(token) {
        if (!localStorage.getItem("score-0")) {
            insertScores()
        }
        for (let i = 0; i < 10; i++) {
            let old;
            try {
                old = JSON.parse(localStorage.getItem("score-" + i));
            }
            catch(ex){
                old = reOrg(i);
            }
            if (old.score < score && localStorage.getItem("score-" + i) != null) {
                let oldScore,
                    pushScore,
                    toR;
                for (let j = i + 1; j <= 10; j++) {
                    if (oldScore == undefined) {
                        oldScore = localStorage.getItem("score-" + j);
                        pushScore = localStorage.getItem("score-" + i)
                    } else {
                        pushScore = oldScore
                    }
                    oldScore = localStorage.getItem("score-" + j);
                    if (j <= 9) {
                        localStorage.setItem("score-" + j, pushScore)
                    }
                }
                localStorage.setItem("score-" + i, JSON.stringify({
                    score: score,
                    diff: settingsStore.difficulty,
                    token: token
                }))
                toR = i;
                i = 10;
                return toR;
            }
        }
    }

    function setSize(canvas) {
        let hbs = [];
        if (!oldSize) {
            oldSize = [ww, wh];
            hbs = [ww / 1800, ww / 1800]
        }
        if (!sizeSet) {
            hbs[0] = ww / oldSize[0];
            hbs[1] = wh / oldSize[1];
            hbr = ww / 1800;
            canvas.width = ww;
            canvas.height = wh - 55;
            sizeSet = true;
            head.w = headW[charSel] * hbr;
            head.h = 197 * hbr;
            head.velX = head.velX * hbs[0];
            head.velY = head.velY * hbs[1];
            head.posX = head.posX * hbs[0];
            head.posY = head.posY * hbs[1];
            if (missile != undefined) {
                for (i = 0; i < missile.length; i++) {
                    missile[i].w = 100 * hbr;
                    missile[i].h = 133 * hbr;
                    missile[i].velX = missile[i].velX * hbs[0];
                    missile[i].velY = missile[i].velY * hbs[1];
                    missile[i].posX = missile[i].posX * hbs[0];
                    missile[i].posY = missile[i].posY * hbs[1]
                }
            }
            if (enemies != undefined) {
                for (i = 0; i < enemies.length; i++) {
                    if (enemies[i].id == "Uffe") {
                        enemies[i].w = 150 * hbr;
                        enemies[i].h = 210 * hbr
                    } else {
                        enemies[i].w = 150 * hbr;
                        enemies[i].h = 229 * hbr
                    }
                    enemies[i].velX = enemies[i].velX * hbs[0];
                    enemies[i].velY = enemies[i].velY * hbs[1];
                    enemies[i].posX = enemies[i].posX * hbs[0];
                    enemies[i].posY = enemies[i].posY * hbs[1]
                }
            }
            oldSize = [ww, wh];
            setTimeout(() => {
                sizeSet = false
            }, 20)
        }
        if (ww / wh > 1.4) {
            selBG = 0
        } else {
            selBG = 1
        }
        if (ww <= 900 && ww > wh || ww <= 900 && wh < 450) {
            $(".mainArea,#main-0,#finish-0,.controlPad,#endPoints").addClass("wide")
        } else {
            $(".mainArea,#main-0,#finish-0,.controlPad,#endPoints").removeClass("wide")
        }
    }

    function EnterSettings() {
        menuIndex = 1;
        $("#settingsWindow").addClass("enabled");
        $("#mainM,#rS").removeClass("enabled");
    }

    function saveUnchanged() {
        modal = true;
        $("#dialogBox-0").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            close: CloseFunction,
            modal: true,
            buttons: {
                "Spara": function () {
                    saveSettings(true);
                    $(this).dialog("close");
                    modal = false
                },
                "Ignorera": function () {
                    saveSettings(false);
                    save = false;
                    modal = false;
                    $(this).dialog("close")
                },
                "Avbryt": function () {
                    $(this).dialog("close");
                    modal = false
                }
            }
        })
    }
    async function sendReq(method, dest, headers, body) {
        try {
            const res = await fetch(`/api/${dest}`, {
                method: method,
                mode: 'cors',
                headers: headers,
                body: body,
            });
            const contentType = res.headers.get("content-type");
            if (res.status >= 200 && res.status < 400) {
                if (contentType && contentType.indexOf("application/json") !== -1) {
                    return res.json()
                } else {
                    return res.text()
                }
            } else {
                throw (res)
            }
        } catch (err) {
            throw err
        }
    }

    function EnterAbout() {
        menuIndex = 1;
        $("#aboutWindow").addClass("enabled");
        $("#mainM,#rS").removeClass("enabled")
    }

    function backfunc() {
        if (menuIndex == 1) {
            $(".setArrow,.subPanel,.subSections").removeClass("enabled");
            if (!start && !gameOver) {
                $("#rS").addClass("enabled")
            }
            if ($("#submitWrapper").hasClass("enabled")) {
                closeSubmit()
            } else {
                $("#mainM").addClass("enabled")
            }
            menuIndex--
        } else if (menuIndex == 2) {
            $(".setArrow,.subPanel").removeClass("enabled");
            if ($("#submitWrapper").hasClass("enabled")) {
                closeSubmit()
            }
            menuIndex--
        } else if (menuIndex == 3) {
            changeKeyBindings(ctrltoChange)
        }
    }
    return {
        //inits the game
        init: id => {
            canvas = document.getElementById(id);
            setSize(canvas);
            if ('serviceWorker' in navigator) {
                $(window).on("load", async () => {
                    try {
                        await navigator.serviceWorker
                            .register("./service-worker.js");
                    }
                    catch (ex) {
                    }
                });
            }
            loadSettings(false);
            context = canvas.getContext('2d');
            $(canvas).focus();
            head.image = new Image();
            head.image.src = "img/" + headImgs[charSel] + ".png";
            head.collision = true;
            head.bounce = 0.6;
            head.id = "annie";
            if (isMobile.any) {
                $("#panel-2").css("display", "none")
            } else {
                $(".controlWrapper").css("display", "none")
            }
            initialValues(false);
            lastTime = (new Date()).getTime();
            render()
        }
    }
}