const cacheName = "v1";

const cacheAssets = [
    'index.html',
    '/css/main.css',
    '/css/jquery-ui.css',
    '/js/game.js',
    '/js/isMobile.js',
    '/js/settings.json',
    '/js/jquery-3.3.1.min.js',
    '/js/hammer.min.js',
    '/js/jquery.hammer.js',
    '/js/jquery.ui.touch-punch.min.js',
    '/js/jquery-ui.min.js',
    '/img/uffe.png',
    '/img/riksdag.jpg',
    '/img/riksdag2.jpg',
    '/img/al.png',
    '/img/jan.png',
    '/img/steffe.png',
    '/img/pause.svg',
    '/img/arrow.svg',
    '/img/play.svg',
    '/img/cross.png',
    '/img/jimmie.png',
    '/img/jonas.png',
    '/img/logo.png'
];

self.addEventListener('install', (e) => {
    e.waitUntil(
        caches.open(cacheName).then(cache => {
            cache.addAll(cacheAssets);
        })
            .then(() =>{
                self.skipWaiting();
            })
    );
});

self.addEventListener('activate', (e) => {
    e.waitUntil(
    caches.keys().then(cacheNames =>{
        return Promise.all(
            cacheNames.map(cache =>{
                if(cache !== cacheName){
                    return caches.delete(cache);
                }
            })
        )
    })
    );
});

self.addEventListener('fetch',e =>{
    e.respondWith(fetch(e.request).catch(()=>{
        caches.match(e.request);
    }));
});