checkHost = require('./checkHost');

module.exports = function(req,body){
    let alias = body.userAlias;
    let hostCorrect = checkHost(req);
    const nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
    if (nonAllowed.test(alias) == true || nonAllowed.test(body.sessionID) == true ||!hostCorrect) {
        return false;
    }
    else{
        return true;
    }
}