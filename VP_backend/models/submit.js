const Joi = require('joi');

module.exports = function validate(input,i) {
    const schema = [{
        sessionID: Joi.string().min(10).max(40).required(),
        userAlias: Joi.string().min(5).max(18).required(),
        difficulty: Joi.number().integer().min(0).max(2).required(),
        captcha: Joi.string().required()
    },
    {
        sessionID: Joi.string().min(10).max(40).regex(/^[a-zA-Z0-9-_]/).required(),
        diff: Joi.number().min(1).max(2)
    },
    {
        sessionID: Joi.string().min(10).max(40).regex(/^[a-zA-Z0-9-_]/).required()
    },
    {
        sessionID: Joi.string().min(10).max(40).regex(/^[a-zA-Z0-9-_]/).required(),
        score: Joi.number().integer().min(1).required(),
        diff: Joi.number().integer().min(0).max(2).required()
    },
    {
        token: Joi.string().regex(/^[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+\.[A-Za-z0-9-_.+/=]*$/).required(),
        userAlias: Joi.string().min(5).max(18).required(),
        captcha: Joi.string().required()
    }
    ]
  
    return Joi.validate(input, schema[i]);
}