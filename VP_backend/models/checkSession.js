checkHost = require('./checkHost');

module.exports = function(req,sessionID){
    const nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
    const hostCorrect = checkHost(req);
    if (nonAllowed.test(sessionID) == true||
    !hostCorrect) {
        return true;
    }
    else{
        return false;
    }
}