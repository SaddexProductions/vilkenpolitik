const bodyParser = require('body-parser');

//local modules
const scores = require('../routes/scores');
const initialize = require('../routes/initialize');
const update = require('../routes/update');
const pause = require('../routes/pause');
const resume = require('../routes/resume');
const end = require('../routes/end');
const error = require('../middleware/errors');

module.exports = function(app){
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use('/api/scores',scores);
    app.use('/api/initialize',initialize);
    app.use('/api/update',update);
    app.use('/api/pause',pause);
    app.use('/api/resume',resume);
    app.use('/api/end',end);
    app.use(error);
}