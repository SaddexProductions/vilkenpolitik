const config = require('config');
const mysql = require('mysql');
const mysqlPassword = config.get('mysqlPassword');

let db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: mysqlPassword,
    database: "SCORES",
    charset: 'utf8mb4'
});

let connection = function () {
    db.connect((err) => {
        if (err) {
            console.log(err);
            throw new Error(err);
        }
    });
}

exports.db = db;
exports.connection = connection;