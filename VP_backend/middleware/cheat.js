module.exports = function(results,req){
    let data = results[0];
    let t1 = data.date;
    let t3 = data.updated;
    let t2 = new Date();

    let Seconds_from_T1_to_T2 = ((t2 - t1) / 1000);
    let Seconds_from_T1_to_T3 = ((t2 - t3) / 1000);
    let Seconds_Between_Dates = Math.floor(Math.abs(Seconds_from_T1_to_T2));
    let Seconds_Between_Dates2 = Math.floor(Math.abs(Seconds_from_T1_to_T3));
    if (req.score > 200 && data.score == 0 || req.score > 200 &&
        (req.score / data.score) > 0.3 || data.ended == 1 ||
        req.score / (Seconds_Between_Dates - data.extraseconds) < 0.92 && req.score > 100 ||
        req.score / (Seconds_Between_Dates - data.extraseconds) > 1.07 && req.score > 100 || req.score > 24000 ||
        data.score > 1000 && data.score / (Seconds_Between_Dates - data.extraseconds) > 75 ||
        (Seconds_Between_Dates2 - data.updateExtraSeconds) > 40 ||
        data.cheated == 1 || data.isGamePaused == 1 || data.similar > 10)
    {
        return true;
    }
    else {
        return false;
    }
}