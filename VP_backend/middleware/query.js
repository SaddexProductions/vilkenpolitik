const mysql = require('mysql');

//local modules
const {db} = require('../startup/db');

module.exports = (sql, someToReturn) => {
    return new Promise((resolve, reject) => {
        db.query(sql, (err, results) => {
            if (err) {
                reject(err);
            }
            else if (!err && someToReturn == true) {
                resolve(results)
            }
            else {
                resolve()
            }
        });
    })
}