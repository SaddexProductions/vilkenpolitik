const SQLquery = require('./query');

module.exports = (score, req) => {
    return new Promise(async (resolve, reject) => {
        let finalSpot;
        let row = [];
        try {
            const results = await SQLquery('SELECT * FROM leaderboard;', true);
            row = results;
        }
        catch(ex){
            reject(ex);
        }
        for (let i = 0; i < 101; i++) {
            let oldScore;
            let pushScore;
            if (row[i] == undefined || row[i] == null) {
                if (i < 100) {
                    finalSpot = (i + 1);
                    let insert = `null, '${req.userAlias}',${score},${req.difficulty},'${req.sessionID}'`;
                    try {
                        await SQLquery(`INSERT INTO leaderboard VALUES (${insert});`, false);
                        i = 101;
                        resolve(finalSpot);
                    }
                    catch(ex){
                        reject(ex);
                    }
                }
                else {
                    resolve(undefined);
                }
            }
            else if (score > row[i].score) {
                finalSpot = (i + 1);
                let k = i;
                i = 101;
                p = pushInto();
                function pushInto() {
                    return new Promise(async (resolve, reject) => {
                        for (let j = k + 1; j < 101; j++) {
                            if (oldScore == undefined) {
                                if (row[j] != undefined) {
                                    oldScore = JSON.parse(JSON.stringify(row[j]));
                                }
                                pushScore = JSON.parse(JSON.stringify(row[k]));
                            }
                            else {
                                pushScore = JSON.parse(JSON.stringify(oldScore));
                            }
                            if (j < 100 && row[j] != undefined) {
                                oldScore = JSON.parse(JSON.stringify(row[j]));

                                try{
                                    await SQLquery('SET autocommit=0;',false);
                                    await SQLquery('START TRANSACTION;', false);
                                }
                                catch(ex){
                                    reject(ex);
                                }
                                try {
                                    let insert = `alias = '${pushScore.alias}', score = ${pushScore.score}, diffID = ${pushScore.diffID}, scoreID = '${pushScore.scoreID}'`;
                                    await SQLquery(`UPDATE leaderboard SET ${insert} WHERE id = ${(j + 1)};`, false);
                                }
                                catch(ex){
                                    reject(ex);
                                }
                            }

                            else if (j < 100 && row[j] == undefined) {
                                try {
                                    await SQLquery('SET autocommit=0;',false);
                                    await SQLquery('START TRANSACTION;', false);
                                }
                                catch(ex){
                                    reject(ex);
                                }

                                try {
                                    let insert = `null, '${pushScore.alias}',${pushScore.score},${pushScore.diffID},'${pushScore.scoreID}'`;
                                    await SQLquery(`INSERT INTO leaderboard VALUES (${insert});`, false);
                                }
                                catch (ex) {
                                    reject(ex);
                                }
                                try {
                                    insert = `alias = '${req.userAlias}', score = ${score}, diffID = ${req.difficulty}, scoreID = '${req.sessionID}'`;
                                    await SQLquery(`UPDATE leaderboard SET ${insert} WHERE id = ${(k + 1)};`, false);
                                    resolve();
                                }
                                catch (ex) {
                                    reject(ex);
                                }
                            }
                            if (row[j] == undefined) {
                                j = 101;
                            }
                        }
                        resolve();
                    });
                }
                p.then(async () => {
                    try {
                        let insert = `alias = '${req.userAlias}', score = ${score}, diffID = ${req.difficulty}, scoreID = '${req.sessionID}'`;
                        await SQLquery(`UPDATE leaderboard SET ${insert} WHERE id = ${(k + 1)};`, false);
                    }
                    catch(ex){
                        reject(ex);
                    }
                    try{
                        await SQLquery('COMMIT;', false);
                        resolve(finalSpot);
                    }
                    catch(ex){
                        reject(ex);
                    }
                }).catch((ex)=>{
                    reject(ex);
                });
            }
        }
    });
}