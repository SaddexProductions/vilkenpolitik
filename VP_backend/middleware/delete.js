const SQLquery = require('./query');

module.exports = function(){
    setInterval(() => {
    SQLquery(`DELETE FROM sessions WHERE updated < (NOW(3) - INTERVAL 40 MINUTE);`, false);
}, 600000);
}