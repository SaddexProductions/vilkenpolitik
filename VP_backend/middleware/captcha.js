const config = require('config');
const request = require('request');
const secretKey = config.get('captchaSecretKey');

module.exports = (req)=>{
    const verifyURL = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;
    return request(verifyURL, (err, response, body) => {
        //if not Successful
        body = JSON.parse(body);
        if (body.success !== undefined && !body.success || response.status >= 400) {
            return false;
        }
        else {
            return true;
        }
    });
}
