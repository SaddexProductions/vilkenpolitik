module.exports = function (results,i) {
    let similar = results[0].similar;
    let primdate = results[0].primdate;
    let secdate = results[0].secdate;
    let terdate = results[0].terdate;
    let firstDis = secdate - terdate;
    let secondDis = primdate - (secdate + results[0].updateExtraSeconds);
    if (secdate != primdate && terdate != secdate && i == 2) {
        if ((firstDis / secondDis) > 0.93 && (firstDis / secondDis) < 1.07) {
            similar++;
        }
        else if ((firstDis / secondDis) < 0.95 && similar > 0 ||
            (firstDis / secondDis) > 1.05 && similar > 0) {
            similar--;
        }
    }
    return similar;
}