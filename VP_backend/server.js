const config = require('config');
const express = require('express');
const app = express();

//local modules
const db = require('./startup/db');

//env variables check
if(!config.get('captchaSecretKey') || !config.get('mysqlPassword') || !config.get('vpSessionKey') || !config.get('vp_jwt')){
    throw new Error('Fatal error - mysqlpassword, vpSessionKey, vp_jwt and/or captcha secret key are not defined - check env variables');
}

require('./startup/logging')();
require('./middleware/delete')();
require('express-async-errors');
require('./startup/routes')(app);
require('./startup/prod')(app);

db.connection();

app.use(function (error, req, res, next) {
    if (error instanceof SyntaxError) {
      return res.status(400).send("Syntaxfel");
    } else {
      next();
    }
});

app.listen(3500);