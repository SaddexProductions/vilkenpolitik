const express = require('express');
const router = express.Router();

//local modules
const SQLquery = require('../middleware/query');
const validate = require('../models/submit');
const validateSession = require('../models/checkSession');
const checkSimilar = require('../middleware/similar');

router.post('/', async (req, res) => {
    const joiResult = validate(req.body, 1);
    const nonAllowed = validateSession(req,req.body.sessionID);
    if (joiResult.error || nonAllowed) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        results = await SQLquery("SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';", true);

        if (results[0].ended == 1 || results[0].cheated == 1 || results[0].isGamePaused == 1) {
            await SQLquery(`UPDATE sessions SET cheated = 1 WHERE sessionID = '${req.body.sessionID}';`, false);
            return res.status(400).send("Misstänkt förfrågan");
        }
        else {
            let i = results[0].dateindex;
            if (i < 2) {
                i++;
            }
            else {
                i = 0;
            }
            let similar = checkSimilar(results,i);

            if(similar == -1){
                return res.status(500).send("Något gick fel");
            }

            let columnToUpdate = ["terdate", "secdate", "primdate"];
            let add = (20 * Math.pow(req.body.diff, 2)) + results[0].score;
            let insert = `sessionID='${results[0].sessionID}', score = ${add}, updated = NOW(3), ${columnToUpdate[i]} = NOW(3), dateindex = ${i}, similar = ${similar}, updateExtraSeconds = 0`;
            await SQLquery(`UPDATE sessions SET ${insert} WHERE sessionID = '${req.body.sessionID}';`, false);
            res.send("Lyckades");
        }
    }
});

module.exports = router;