const express = require('express');
const session = require('express-session');
const router = express.Router();

//local modules
const SQLquery = require('../middleware/query');

router.use(session({
    secret: 'a46AyUFfhopqW714iJKjfaMnnY',
    resave: false,
    saveUninitialized: true
}));

router.post('/', async (req, res) => {
    let sql = "SELECT* FROM sessions WHERE sessionID = '" + req.sessionID + "';";
    const data = await SQLquery(sql, true);
    if (data[0] == null || data[0] == undefined) {
        sql = `INSERT INTO sessions VALUES ('${req.sessionID}',0,NOW(3),NOW(3),NOW(3),NOW(3),NOW(3),0,0,0,0,0,NOW(3),0,0);`
    }
    else {
        let insert = `score = 0, date = NOW(3), updated = NOW(3),primdate = NOW(3), secdate = NOW(3), terdate = NOW(3), cheated = 0, dateindex = 0, ended = 0, similar = 0, isGamePaused = 0, extraseconds = 0, updateExtraSeconds = 0`;
        sql = `UPDATE sessions SET ${insert} WHERE sessionID = '${req.sessionID}';`
    }
    await SQLquery(sql, false);
    res.json({ "sessionID": req.sessionID, "msg": "success" });
});

module.exports = router;