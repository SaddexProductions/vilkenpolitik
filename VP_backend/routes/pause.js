const express = require('express');
const router = express.Router();

//local modules
const SQLquery = require('../middleware/query');
const validate = require('../models/submit');
const validateSession = require('../models/checkSession');

router.post('/', (req, res) => {
    const joiResult = validate(req.body, 2);
    const nonAllowed = validateSession(req,req.body.sessionID);
    if (joiResult.error || nonAllowed) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        setTimeout(async () => {
            await SQLquery(`UPDATE sessions SET paused = NOW(3), isGamePaused = 1 WHERE sessionID = '${req.body.sessionID}';`, false);
        }, 300);
        res.status(200).send("Lyckades");
    }
});

module.exports = router;