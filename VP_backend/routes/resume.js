const express = require('express');
const router = express.Router();

//local modules
const SQLquery = require('../middleware/query');
const validate = require('../models/submit');
const validateSession = require('../models/checkSession');

router.post('/', (req, res) => {
    const joiResult = validate(req.body, 2);
    const nonAllowed = validateSession(req,req.body.sessionID);
    if (joiResult.error || nonAllowed) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        const p = SQLquery("SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';", true);
        p.then(async (results)=>{
            data = results[0];
            let t1 = data.paused;
            let t2 = new Date();
            let Seconds_from_T1_to_T2 = ((t2 - t1) / 1000);
            let Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2) + data.extraseconds;
            let extra = Math.abs(Seconds_from_T1_to_T2) + data.updateExtraSeconds;
            await SQLquery(`UPDATE sessions SET extraseconds = ${Seconds_Between_Dates}, isGamePaused = 0, updateExtraSeconds = ${extra} WHERE sessionID = '${req.body.sessionID}';`, false);
            res.send("Lyckades");
        });
    }
});

module.exports = router;