const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('config');
const router = express.Router();

//local modules
const validate = require('../models/submit');
const escapeChars = require('../models/checkEscape');
const scoreInsert = require('../middleware/placeScore');
const captcha = require('../middleware/captcha');
const SQLquery = require('../middleware/query');

router.get('/', async (req, res) => {
    const data = await SQLquery('SELECT leaderboard.alias, leaderboard.score, leaderboard.scoreID, difficulty.diff FROM leaderboard INNER JOIN difficulty ON leaderboard.diffID = difficulty.id ORDER BY leaderboard.id;', true);
    res.json(data);
});

router.post('/', async (req, res) => {
    let scoreSchema = 0;
    if(req.body.token) scoreSchema = 4;

    const joiResult = validate(req.body, scoreSchema);
    if (joiResult.error) {
        return res.status(400).json({msg: "Välj captcha"});
    }

    let escape = escapeChars(req, req.body);

    if (escape == false) {
        return res.status(400).json({msg: "Otillåtna tecken i förfrågan"});
    }

    const captchaSuccess = await captcha(req);
    if (!captchaSuccess) {
        return res.status(400).send({msg: "Captcha-verifikationen misslyckades"});
    }
    else {
        if (req.body.token) {
            try {
                let decoded = jwt.verify(req.body.token,config.get("vp_jwt"));
                results = await SQLquery(`SELECT* FROM leaderboard WHERE scoreID = '${decoded.sessionID}';`,true);
                if(results.length > 0) return res.status(400).json({msg: "Resultatet finns redan på servern!"});

                decoded.userAlias = req.body.userAlias;
                if(!decoded.difficulty) decoded.difficulty = decoded.diff;

                const finalSpot = await scoreInsert(decoded.score, decoded);
                if (finalSpot != undefined) {
                    res.json({ "success": true, "msg": `Ranked on spot:${finalSpot}` });
                }
                else {
                    res.json({ "success": true, "msg": `Unranked` });
                }
            }
            catch (ex) {
                return res.status(400).json({ msg: "Felaktig token" });
            }
        }
        else {
            results = await SQLquery("SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';", true);
            let data = results[0];
            if (data.ended == 1) {
                const finalSpot = await scoreInsert(data.score, req.body);
                await SQLquery(`UPDATE sessions SET score = 0, ended = 0 WHERE sessionID = '${req.body.sessionID}';`, false);
                if (finalSpot != undefined) {
                    res.json({ "success": true, "msg": `Ranked on spot:${finalSpot}` });
                }
                else {
                    res.json({ "success": true, "msg": `Unranked` });
                }
            }
            else {
                res.status(400).json({msg: "Misstänkt förfrågan"});
            }
        }
    }
});

module.exports = router;