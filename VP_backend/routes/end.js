const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('config');
const Math = require('mathjs');

//local modules
const router = express.Router();
const checkCheat = require('../middleware/cheat');
const SQLquery = require('../middleware/query');
const validate = require('../models/submit');
const validateSession = require('../models/checkSession');

router.post('/', (req, res) => {
    const joiResult = validate(req.body, 3);
    const nonAllowed = validateSession(req);
    if (joiResult.error || nonAllowed) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        setTimeout(async () => {
            const results = await SQLquery("SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';", true);

            const hasCheated = checkCheat(results,req.body);

             if(hasCheated){
                    
                await SQLquery(`UPDATE sessions SET cheated = 1 WHERE sessionID = '${req.body.sessionID}';`, false);
                res.status(400).send("Misstänkt förfrågan");
            }
            else {
                let add = req.body.score + results[0].score;
                let insert = `score = ${add}, updated = NOW(3), ended = 1`;
                await SQLquery(`UPDATE sessions SET ${insert} WHERE sessionID = '${req.body.sessionID}';`, false);
                
                const token = await jwt.sign({
                    score: add,
                    difficulty: req.body.diff,
                    sessionID: req.body.sessionID + Math.random().toString(36).substring(2, 15)
                },config.get("vp_jwt"));

                res.status(200).json({token: token});
            }
        }, 100);
    }
});

module.exports = router;