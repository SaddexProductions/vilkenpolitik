const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const request = require('request');
const mysql = require('mysql');
const Joi = require('joi');
let path = require('path');
const session = require('express-session');
const app = express();
app.use(session({
    secret: 'a46AyUFfhopqW714iJKjfaMnnY',
    resave: false,
    saveUnized: true
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('short'));
setInterval(() => {
    sql = `DELETE FROM sessions WHERE updated < (NOW(3) - INTERVAL 40 MINUTE)`;
    db.query(sql, (err, results) => {
        if (err) {
            console.log(err);
        }
    });
}, 600000);

let db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "smugg3l_OLOL",
    database: "SCORES",
    charset: 'utf8mb4'
});
db.connect((err) => {
    if (err) {
        console.log(err);
        throw err;
    }
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/../index.html'));
});
app.get('/api', (req, res) => {
    return res.send("Try extending the url");
});
app.get('/api/scores', (req, res) => {
    let sql = 'SELECT leaderboard.alias, leaderboard.score, difficulty.diff FROM leaderboard INNER JOIN difficulty ON leaderboard.diffID = difficulty.id ORDER BY leaderboard.id;';
    new Promise((resolve, reject) => {
        let query = db.query(sql, (err, results) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(results);
            }
        });
    }).then((data) => {
        return res.json(data);
    }).catch((err) => {
        console.log(err);
        return res.json({ "success": false, "msg": "Misslyckades att ladda databasen." });
    });
});
app.get('/api/initialize', (req, res) => {
    let sql = "SELECT* FROM sessions WHERE sessionID = '" + req.sessionID + "';";
    new Promise((resolve, reject) => {
        db.query(sql, (err, results) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(results);
            }
        });
    }).then((data) => {
        if (data[0] == null || data[0] == undefined) {
            sql = `INSERT INTO sessions VALUES ('${req.sessionID}',0,NOW(3),NOW(3),NOW(3),NOW(3),NOW(3),0,0,0,0,0,NOW(3),0,0);`
            return sql;
        }
        else {
            let insert = `score = 0, date = NOW(3), updated = NOW(3),primdate = NOW(3), secdate = NOW(3), terdate = NOW(3), cheated = 0, dateindex = 0, ended = 0, similar = 0, isGamePaused = 0, extraseconds = 0, updateExtraSeconds = 0`;
            sql = `UPDATE sessions SET ${insert} WHERE sessionID = '${req.sessionID}';`
            return sql;
        }
    }).then((sql) => {
        SQLquery(sql);
        return res.json({ "sessionID": req.sessionID, "msg": "success" });
    }).catch((err) => {
        console.log(err);
        return res.status(500);
    });
});
let SQLquery = (sql) => {
    db.query(sql, (err, results) => {
        if (err) {
            console.log(err);
            throw err;
        }
    });
}
app.post('/api/scores', (req, res) => {
    if (
        req.body.captcha === undefined ||
        req.body.captcha === '' ||
        req.body.captcha === null
    ) {
        return res.json({ "success": false, "msg": "Verifiera captcha" });
    }
    const schema = {
        sessionID: Joi.string().min(10).max(40).required(),
        userAlias: Joi.string().min(5).max(18).required(),
        difficulty: Joi.number().integer().min(0).max(2).required(),
        captcha: Joi.string()
    }
    const joiResult = Joi.validate(req.body, schema);
    if (joiResult.error) {
        return res.status(400);
    }
    let alias = req.body.userAlias;
    let diffID = req.body.difficulty;
    const nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
    if (nonAllowed.test(alias) == true || nonAllowed.test(req.body.sessionID) == true) {
        return res.status(400);
    }

    //secret key
    const secretKey = '6LdEeX8UAAAAALJT7eq9ShHsX4_I-tL9WJVZa4AZ';

    //verifyurl
    const verifyURL = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;

    //make request to verifyurl
    request(verifyURL, (err, response, body) => {
        //if not Successful
        body = JSON.parse(body);
        if (body.success !== undefined && !body.success || response.status >= 400) {
            return res.json({ "success": false, "msg": "Captcha-verifikationen misslyckades" });
        }
        else {
            let sql = "SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';";
            db.query(sql, (err, results) => {
                if (err) {
                    return res.json({ "success": false, "msg": "Sessionsid finns ej" });
                }
                let data = results[0];
                if (data.ended == 1) {
                    scoreInsert(data);
                    sql = `UPDATE sessions SET score = 0, ended = 0 WHERE sessionID = '${req.body.sessionID}';`;
                    db.query(sql, (err, results) => {
                        if (err) {
                            throw err;
                        }
                    });
                }
                else {
                    res.status(400).send("Misstänkt förfrågan");
                }
            });
        }
        scoreInsert = (data) => {
            let finalSpot;
            let sql = 'SELECT * FROM leaderboard';
            let row = [];
            db.query(sql, (err, results) => {
                if (err) {
                    return res.json({ "success": false, "msg": "Misslyckades att ladda databasen." });
                }
                row = results;
                for (let i = 0; i < 101; i++) {
                    let oldScore;
                    let pushScore;
                    if (row[i] == undefined || row[i] == null) {
                        if (i < 100) {
                            finalSpot = (i + 1);
                            insertIntoDatabase({ alias: req.body.userAlias, score: data.score, diffID: req.body.difficulty });
                            i = 101;
                            returnFunc(finalSpot);
                        }
                        else {
                            elseReturn();
                        }
                    }
                    else if (data.score > row[i].score) {
                        finalSpot = (i + 1);
                        for (let j = i + 1; j < 101; j++) {
                            if (oldScore == undefined) {
                                if (row[j] != undefined) {
                                    oldScore = JSON.parse(JSON.stringify(row[j]));
                                }
                                pushScore = JSON.parse(JSON.stringify(row[i]));
                            }
                            else {
                                pushScore = JSON.parse(JSON.stringify(oldScore));
                            }
                            if (j < 100 && row[j] != undefined) {
                                oldScore = JSON.parse(JSON.stringify(row[j]));
                                alterDatabase(pushScore, (j + 1));
                            }
                            else if (j < 100 && row[j] == undefined) {
                                insertIntoDatabase(pushScore);
                            }
                            alterDatabase({ alias: alias, score: data.score, diffID: diffID }, (i + 1));
                            if (row[j] == undefined) {
                                j = 101;
                            }
                        }
                        i = 101;
                        returnFunc(finalSpot);
                    }
                }
            });
        }
        returnFunc = function (data) {
            if (data != undefined) {
                return res.json({ "success": true, "msg": `Ranked on spot:${data}` });
            }
            else {
                return res.json({ "success": true, "msg": `Unranked` });
            }
        }
        elseReturn = function () {
            return res.json({ "success": true, "msg": `Unranked` });
        }
    });
});
app.post('/api/update', (req, res) => {
    const schema = {
        sessionID: Joi.string().min(10).max(40).regex(/^[a-zA-Z0-9-_]/).required(),
        diff: Joi.number().min(1).max(2)
    }
    const joiResult = Joi.validate(req.body, schema);
    const nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
    if (joiResult.error || nonAllowed.test(req.body.sessionID) == true ||
        req.headers.host != "vilkenpolitik.se" || !req.headers.host) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        let sql = "SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';";
        db.query(sql, (err, results) => {
            if (err) {
                throw err;
            }
            if (results[0].ended == 1 || results[0].cheated == 1 || results[0].isGamePaused == 1) {
                let sql = `UPDATE sessions SET cheated = 1 WHERE sessionID = '${req.body.sessionID}';`;
                db.query(sql, (err, results) => {
                    if (err) {
                        throw err;
                    }
                });
                return res.status(400).send("Misstänkt förfrågan");
            }
            else {
                let i = results[0].dateindex;
                if (i < 2) {
                    i++;
                }
                else {
                    i = 0;
                }
                let similar = results[0].similar;
                let primdate = results[0].primdate;
                let secdate = results[0].secdate;
                let terdate = results[0].terdate;
                let firstDis = secdate - terdate;
                let secondDis = primdate - (secdate + results[0].updateExtraSeconds);
                if (secdate != primdate && terdate != secdate && i == 2) {
                    if ((firstDis / secondDis) > 0.93 && (firstDis / secondDis) < 1.07) {
                        similar++;
                    }
                    else if ((firstDis / secondDis) < 0.95 && similar > 0 ||
                        (firstDis / secondDis) > 1.05 && similar > 0) {
                        similar--;
                    }
                }

                let columnToUpdate = ["terdate", "secdate", "primdate"];
                let add = (20 * Math.pow(req.body.diff, 2)) + results[0].score;
                let insert = `sessionID='${results[0].sessionID}', score = ${add}, updated = NOW(3), ${columnToUpdate[i]} = NOW(3), dateindex = ${i}, similar = ${similar}, updateExtraSeconds = 0`;
                sql = `UPDATE sessions SET ${insert} WHERE sessionID = '${req.body.sessionID}';`;
                db.query(sql, (err, results) => {
                    if (err) {
                        throw err;
                    }
                });
                res.status(200).send("Lyckades");
            }
        });
    }
});
app.post('/api/pause', (req, res) => {
    const schema = {
        sessionID: Joi.string().min(10).max(40).regex(/^[a-zA-Z0-9-_]/).required()
    }
    const joiResult = Joi.validate(req.body, schema);
    const nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
    if (joiResult.error || nonAllowed.test(req.body.sessionID) == true || req.headers.host != "vilkenpolitik.se"
        || !req.headers.host) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        setTimeout(() => {
            let sql = `UPDATE sessions SET paused = NOW(3), isGamePaused = 1 WHERE sessionID = '${req.body.sessionID}';`
            db.query(sql, (err, results) => {
                if (err) {
                    throw err;
                }
            });
        }, 300);
        res.status(200).send("Lyckades");
    }
});
app.post('/api/resume', (req, res) => {
    const schema = {
        sessionID: Joi.string().min(10).max(40).regex(/^[a-zA-Z0-9-_]/).required()
    }
    const joiResult = Joi.validate(req.body, schema);
    const nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
    if (joiResult.error || nonAllowed.test(req.body.sessionID) == true || req.headers.host != "vilkenpolitik.se"
        || !req.headers.host) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        let sql = "SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';";
        db.query(sql, (err, results) => {
            if (err) {
                throw err;
            }
            let data = results[0];
            let t1 = data.paused;
            let t2 = new Date();
            let Seconds_from_T1_to_T2 = ((t2 - t1) / 1000);
            let Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2) + data.extraseconds;
            let extra = Math.abs(Seconds_from_T1_to_T2) + data.updateExtraSeconds;
            sql = `UPDATE sessions SET extraseconds = ${Seconds_Between_Dates}, isGamePaused = 0, updateExtraSeconds = ${extra} WHERE sessionID = '${req.body.sessionID}';`
            db.query(sql, (err, results) => {
                if (err) {
                    throw err;
                }
            });
        });
        res.status(200).send("Lyckades");
    }
});
app.post('/api/end', (req, res) => {
    const schema = {
        sessionID: Joi.string().min(10).max(40).regex(/^[a-zA-Z0-9-_]/).required(),
        score: Joi.number().integer().min(1).required(),
    }
    const joiResult = Joi.validate(req.body, schema);
    const nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/;
    if (joiResult.error || nonAllowed.test(req.body.sessionID) == true
        || req.headers.host != "vilkenpolitik.se" || !req.headers.host) {
        return res.status(400).send("Felaktigt format");
    }
    else {
        setTimeout(() => {
            let sql = "SELECT* FROM sessions WHERE sessionID = '" + req.body.sessionID + "';";
            db.query(sql, (err, results) => {
                if (err) {
                    throw err;
                }
                let data = results[0];
                let t1 = data.date;
                let t3 = data.updated;
                let t2 = new Date();

                let Seconds_from_T1_to_T2 = ((t2 - t1) / 1000);
                let Seconds_from_T1_to_T3 = ((t2 - t3) / 1000);
                let Seconds_Between_Dates = Math.floor(Math.abs(Seconds_from_T1_to_T2));
                let Seconds_Between_Dates2 = Math.floor(Math.abs(Seconds_from_T1_to_T3));
                if (req.body.score > 200 && data.score == 0 || req.body.score > 200 &&
                    (req.body.score / data.score) > 0.3 || data.ended == 1 ||
                    req.body.score / (Seconds_Between_Dates - data.extraseconds) < 0.92 && req.body.score > 100 ||
                    req.body.score / (Seconds_Between_Dates - data.extraseconds) > 1.07 && req.body.score > 100 || req.body.score > 24000 ||
                    data.score > 1000 && data.score / (Seconds_Between_Dates - data.extraseconds) > 75 ||
                    (Seconds_Between_Dates2 - data.updateExtraSeconds) > 40 ||
                    data.cheated == 1 || data.isGamePaused == 1 || data.similar > 10) {
                    let sql = `UPDATE sessions SET cheated = 1 WHERE sessionID = '${req.body.sessionID}';`;
                    db.query(sql, (err, results) => {
                        if (err) {
                            throw err;
                        }
                    });
                    return res.status(400).send("Misstänkt förfrågan");
                }
                else {
                    let add = req.body.score + data.score;
                    let insert = `score = ${add}, updated = NOW(3), ended = 1`
                    sql = `UPDATE sessions SET ${insert} WHERE sessionID = '${req.body.sessionID}';`
                    db.query(sql, (err, results) => {
                        if (err) {
                            throw err;
                        }
                    });
                    res.status(200).send("Lyckades");
                }
            });
        }, 100);
    }
});
app.listen(3500);

let insertIntoDatabase = (data) => {
    let insert = `null, '${data.alias}',${data.score},${data.diffID}`;
    sql = `INSERT INTO leaderboard VALUES (${insert});`;
    db.query(sql, (err, results) => {
        if (err) {
            throw err;
        }
    });
}
let alterDatabase = (data, spot) => {
    let insert = `alias = '${data.alias}', score = ${data.score}, diffID = ${data.diffID}`;
    sql = `UPDATE leaderboard SET ${insert} WHERE id = ${spot};`;
    db.query(sql, (err, results) => {
        if (err) {
            throw err;
        }
    });
}