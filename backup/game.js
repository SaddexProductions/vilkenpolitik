$(document).ready(function () { var myGame = new Game(); myGame.init('gameArea'); $("#imgWrapper").html("<a href='https://www.saddexproductions.com?lang=3' target='_blank'><img src='img/logo.png'></a>"); var urlLocation = window.location.href; if (urlLocation.indexOf("www") > -1) { window.location.replace("https://vilkenpolitik.se") } }); var onloadCallback = function () { grecaptcha.render('captcha', { 'sitekey': '6LdEeX8UAAAAADmJUFAsdjMl235HALGAhjMw85Et' }) }
var Game = function () {
    const v = 20; const vMax = 500; const decay = 0.8; const c = 60; const c2 = 20; const mS = -500; const scorePow = 2; const translateNonNum = [105, 99, 97, 103, 100, 104, 102, 98]; var canvas, context; var charSel = 0; var pause = !1; var hbr = 1; var sizeSet = !1; var errors; var dontResume = !0; var sessionID; var menuIndex = 0; var ww = window.innerWidth; var wh = window.innerHeight; var diff = 1; var diffTrans = ["Opposition", "Minoritetsregering", "Majoritetsregering"]; var hasShot; var hasRecPaused = !1; var head = {}; var map = {}; var missile; const mouseButtons = ["LMB", "MMB", "RMB"]; const ctrlKeys = ["LEFT<br>SHIFT", "LEFT<br>CTRL", "LALT", "PAUSE<br>BREAK"]; const ctrlKeys2 = ["RIGHT<br>SHIFT", "RIGHT<br>CTRL", "ALT<br>GR"]; const sweKeys = ["Ö", "´", "§", "Å", "Ä", "' "]; const rightSpecialKeys = ["PAGE<br>UP", "PAGE<br>DOWN", "END", "HOME", "&lArr;", "&uArr;", "&rArr;", "&dArr;"]; const insDel = ["INS", "DEL"]; const numpadSymb = ["NUMP<br>*", "NUMP<br>+"]; const headW = [187, 142]; var changeControls = !1; var ctrltoChange; var backPressed = !1; var musicVolume; var sfxVolume; var kB = [65, 68, 87, 83, 1, 3]; var life; var modal = !1; var eTime; var enemies; var score; var addScore; var start = !0; var hasRecentlySaved = !1; var lastHit; var local = !0; var gameOver; var exit = !1; var settingsStore; var newSettings; var scoreAlreadySubmitted = !1; var mousePos = [2]; var SDimage = new Image(); var Vimage = new Image(); var SteffeImg = new Image(); var UffeImg = new Image(); var bg = new Image(); var headImgs = ['al', 'jan']; var stopped = 750; var clock; var key; var hasTriedTwice = !1; var music = document.getElementById("music"); var sample = document.getElementById("sample"); var bumpSound = document.getElementById("sfx-0"); var price = document.getElementById("sfx-1"); var kabbel = document.getElementById("sfx-2"); var felSak = document.getElementById("sfx-3"); var annie = document.getElementById("sfx-4"); var jan = document.getElementById("sfx-5"); var nej = [annie, jan]; var sfx = [bumpSound, price, kabbel, felSak, nej[charSel]]; music.volume = 0.5; music.loop = !0; SDimage.src = 'img/jimmie.png'; Vimage.src = 'img/jonas.png'; bg.src = 'img/riksdag.jpg'; SteffeImg.src = 'img/steffe.png'; UffeImg.src = 'img/uffe.png'; var currentTime, lastTime, deltaTime; $(document).on('keydown mousedown', function (e) {
        key = e.which; if (key == 222 && (e.key).toUpperCase() != "Ä" || key == 192 && (e.key).toUpperCase() != "Ö") { key++ }
        else if (key >= 13 && key <= 18 && e.originalEvent.location > 2) { key = key + 220 }
        else if (key == 12 && e.originalEvent.location == 3) { key = 101 }
        else if (key >= 33 && key <= 40 && e.originalEvent.location == 3) { key = translateNonNum[key - 33] }
        map[key] = !0; if (map[key] == kB[4] || map[key] == kB[5]) { mousePos = [e.pageX, e.pageY] }
        if (changeControls == !0) { setBindings(!0, key) }
    }).on('keyup mouseup', function (e) {
        key = e.which; if (key == 222 && (e.key).toUpperCase() != "Ä" || key == 192 && (e.key).toUpperCase() != "Ö") { key++ }
        else if (key >= 13 && key <= 18 && e.originalEvent.location > 2) { key = key + 220 }
        else if (key >= 33 && key <= 40 && e.originalEvent.location == 3) { key = translateNonNum[key - 33] }
        map[key] = !1
    }); $(".restart").mousedown(function (e) { if (e.button == 0) { dontResume = !0; initializeServer(0); scoreAlreadySubmitted = !1; $("#lives").html(""); $("#playpause").addClass("enabled"); music.pause(); music.currentTime = 0; playMusic(); PauseGame(!0); initialValues(!1); hasShot = !0; coolDown(200); setTimeout(function () { dontResume = !1 }, 400) } }); $("#alias").on("keydown", function (e) { if (e.which == 13) { e.preventDefault(); if (gameOver == !0) { postSubmit(settingsStore.difficulty) } } }); $(document).on("mousedown", function (e) {
        if (e.button == 0 && $("#submitWrapper").hasClass("enabled")) { closeSubmit() }
        if (e.button == 2) { e.preventDefault() }
    }); $(".resume").mousedown(function (e) {
        if (e.button == 0) {
            hasShot = !0; coolDown(200); if (start == !0) {
                dontResume = !0; initializeServer(0); setTimeout(function () { dontResume = !1 }, 400); var cookie = document.cookie; if (cookie.indexOf("true") == -1) {
                    $(".instructions").addClass("enabled"); for (var i = 0; i < 6; i++) {
                        var value = $("#bS-" + i).text(); if (value == "LMB" || value == "RMB") { $("#iC-" + i).html("<img src='img/" + value.toLowerCase() + ".svg'>") }
                        else { $("#iC-" + i).html(value) }
                    }
                    setTimeout(function () { $(".instructions").removeClass("enabled"); setTimeout(function () { $(".instructions").css("display", "none") }, 400) }, 8000); createCookie()
                }
                else { $(".instructions").css("display", "none") }
                $("#playpause").addClass("enabled"); playMusic(); start = !1; $(".logo").addClass("started"); $("#pausedGame").html("Pausat"); $(".resume").html("Fortsätt")
            }
            PauseGame(!0)
        }
        $("header").on("mousedown", function (e) { e.stopPropagation() }); $("#playpause").on("mousedown", function (e) { if (e.button == 0) { PauseGame(!0) } })
    }); $(".settings").mousedown(function (e) { if (e.button == 0) { EnterSettings() } }); $(".about").mousedown(function (e) { if (e.button == 0) { EnterAbout() } }); $(".cross").on("mousedown", function (e) { if (e.button == 0) { $(".instructions").removeClass("enabled"); setTimeout(function () { $(".instructions").css("display", "none") }, 400) } }); $(".back").on("mousedown", function (e) { if (e.button == 0 && $(this).attr('id') != "back") { backfunc() } }); $(window).bind('resize', function () { ww = window.innerWidth; wh = window.innerHeight; setSize(document.getElementById('gameArea')) }); $("#back").mousedown(function (e) {
        if (e.button == 0) {
            if (JSON.stringify(newSettings) != JSON.stringify(settingsStore)) { saveUnchanged() }
            else { 
                menuIndex = 1; backfunc() }
            }
    }); $("#default").on("mousedown", function (e) { e.stopPropagation(); if (e.button == 0) { revertToDefault() } }); $(".mainMenu").on("mousedown", function (e) { if (e.button == 0) { mainMenu() } }); $(document).contextmenu(function (e) { e.preventDefault() }); $(".panel").on("mousedown", function (e) { if (e.button == 0) { var id = ($(e.originalEvent.target).attr('id')).split('-')[1]; toggleMenu(id) } }); $(".submit").on("mousedown", function (e) { e.stopPropagation(); if (e.button == 0) { submitWindow() } }); $(".submitWindow, .sliderHolder").on("mousedown", function (e) { e.stopPropagation() }); $(".Sline").on("mousedown"), function (e) { if (e.button == 0) { var id = ($(e.originalEvent.target).attr('id')).split('-')[1]; toggleMenu(id) } }
    $(".control").on("mousedown", function (e) { e.stopPropagation(); if (changeControls == !0) { setBindings(!0, e.which) } }); $("#submitToServer").on("mousedown", function (e) { if (e.button == 0) { if (gameOver == !0) { postSubmit(settingsStore.difficulty) } } }); $(".change").on("mousedown", function (e) { e.stopPropagation(); if (e.button == 0) { var id = ($(e.originalEvent.target).attr('id')).split('-')[1]; backPressed = !0; setTimeout(function () { backPressed = !1 }, 40); if (ctrltoChange == id && changeControls == !0 || ctrltoChange != id && changeControls == !1 || ctrltoChange == id && changeControls == !1) { changeKeyBindings(id) } } }); $(".points").on("mousedown", function (e) { if (e.button == 0) { loadScores() } }); $("#scoreCancel").on("mousedown", function (e) { if (e.button == 0) { closeSubmit() } }); $("#showLeaderBoard").on("mousedown", function (e) {
        if (e.button == 0) {
            if (local == !0) { loadLeaderBoard(); $("#localScoreBoard").addClass("disabled"); $("#showLeaderBoard").html("Lokala resultat"); $("#resultType").html("Global Topp 100"); $(".statusWindow").addClass("enabled"); $(".progressBar").progressbar({ value: 0 }) }
            else { $("#globalScoreBoard").addClass("disabled"); $("#globalScoreBoard").html("<tr id='tr_2--1'><th>Rank</th><th>Alias</th><th>Poäng</th><th>Svårighetsgrad</th></tr>"); $("#localScoreBoard").removeClass("disabled"); $("#showLeaderBoard").html("Global Topp 100"); $("#resultType").html("Lokala resultat"); $(".statusWindow").removeClass("enabled") }
            local = !local
        }
    }); var createCookie = function () { value = "notFirsttime: true"; var date = new Date(); date.setTime(date.getTime() + (90 * 24 * 60 * 60 * 1000)); var expires = "; expires=" + date.toGMTString(); document.cookie = value + expires + "; path=/" }
    var loadLeaderBoard = function () {
        $("#progressBar").removeClass("disabled"); $("#retryingIn").removeClass("disabled"); fetch('/api/scores/', { method: 'GET', headers: { 'Accept': 'application/json, text/plain', 'Content-type': 'application/json', 'cache': !1 } }).then((res) => {
            if (res.status >= 200 && res.status < 400) { return res.json() }
            else { throw (res) }
        }).then((data) => {
            $(".progressBar").removeClass("disabled"); $(".errorLoading").removeClass("enabled"); for (var i = 0; i < data.length; i++) { $("#globalScoreBoard").append("<tr><th class='firstrow'>" + (i + 1) + "<th class='secrow'>" + data[i].alias + "</th><th class='thirdrow'>" + data[i].score + "</th><th class='fourthrow'>" + data[i].diff + "</tr>"); $(".progressBar").progressbar("value", (i / data.length) * 100); $("#progress-label").text(((i / data.length) * 100) + "%") }
            $(".statusWindow").removeClass("enabled"); $("#progress-label").text("Laddar..."); $("#globalScoreBoard").removeClass("disabled")
        }).catch((error) => {
            var countdown = 5; $(".progressBar").addClass("disabled"); $(".errorLoading").addClass("enabled"); $("#errorCode").html(`${error.status} ${error.statusText}`); if (hasTriedTwice == !1) { $("#nrOfSecsLeft").html(countdown); var retry = setInterval(() => { if (countdown >= 0) { countdown--; $("#nrOfSecsLeft").html(countdown) } }, 1000); setTimeout(() => { hasTriedTwice = !0; loadLeaderBoard(); clearInterval(retry) }, 5000) }
            else { $("#retryingIn").addClass("disabled"); setTimeout(() => { hasTriedTwice = !1 }, 1000) }
        })
    }
    var postSubmit = function (diff) {
        var isDiffNumber = jQuery.isNumeric(diff); var alias = $("#alias").val(); var captcharesponse = $(".g-recaptcha-response").val(); var nonAllowed = /[ @#$%^&*()+\=\[\]{};':"\\|,.<>\/]/; if (nonAllowed.test(alias) == !1 && alias != null && alias != "" && alias != undefined && alias.length >= 5 && alias.length < 18 && isDiffNumber == !0 && (diff + 1) % 1 == 0 && errors == 0) {
            fetch('/api/scores/', { method: 'POST', headers: { 'Accept': 'application/json, text/plain', 'Content-type': 'application/json' }, body: JSON.stringify({ userAlias: alias, sessionID: sessionID, captcha: captcharesponse, difficulty: diff }) }).then((res) => {
                if (res.status >= 200 && res.status < 400) { return res.json() }
                else { throw (res) }
            }).then((data) => {
                if (data.success == !0) { $("#status").html("Resultatet skickades till servern!"); $("#status").css("color", "#006400"); setTimeout(function () { menuIndex = 0; closeSubmit(); scoreAlreadySubmitted = !0; scoreBoardResult(data.msg, alias) }, 800) }
                else { $("#status").html(data.msg); $("#status").css("color", "#f00") }
            }).catch((error) => { $("#status").html(`${error.status} ${error.statusText}`); $("#status").css("color", "#f00") })
        }
        else { $("#status").html("Alias måste vara mellan 5-18 tecken och får endast innehålla symboler som ?,!,- och _."); $("#status").css("color", "#f00") }
    }
    var scoreBoardResult = function (msg, alias) { if (msg != "Unranked") { var spot = msg.split(':')[1]; $("#diaDesc-3").html(`Ditt resultat med aliaset ${alias} hamnade på plats ${spot} på Topp 100!`); $("#dialogBox-3").dialog({ resizable: !1, height: "auto", width: 400, modal: !0, buttons: { "Okej": function () { $(this).dialog("close") } } }) } }
    var CloseFunction = function () { modal = !1 }
    var submitWindow = function () {
        if (scoreAlreadySubmitted == !1 && errors == 0) { $("#submitWrapper").addClass("enabled"); $("#spanPoints").html(score); $("#alias").html(""); menuIndex = 1; $(".scoreSubmit").append("<div id='captcha'></div>"); onloadCallback() }
        else {
            if (errors == 0) { $("#diaDesc-3").html("Du har redan skickat detta resultat till servern!") }
            else { $("#diaDesc-3").html("Detta resultat kunde inte valideras som autentiskt av servern!") }
            $("#dialogBox-3").dialog({ resizable: !1, height: "auto", width: 400, modal: !0, buttons: { "Okej": function () { $(this).dialog("close") } } })
        }
    }
    var closeSubmit = function () { $("#submitWrapper").removeClass("enabled"); $("#alias").val(""); $("#status").html(""); $("#captcha").remove() }
    var loadScores = function () {
        $("#mainM").removeClass("enabled"); $("#scoreBoard").addClass("enabled"); $("#rS").removeClass("enabled"); menuIndex = 1; if (local == !0) {
            $("#localScoreBoard").html("<tr id='tr--1'><th>Rank</th><th>Poäng</th><th>Svårighetsgrad</th></tr>"); insertScores(); for (var i = 0; i < localStorage.length - 1; i++) {
                var scoreDiff = localStorage.getItem("score-" + i).split('-')[1]; var Output = []; if (scoreDiff > 2) { Output = ["N/A", "N/A"] }
                else { Output = [localStorage.getItem("score-" + i).split('-')[0], diffTrans[scoreDiff]] }
                $("#localScoreBoard").append("<tr><th class='firstrow'>" + (i + 1) + "<th class='secrow'>" + Output[0] + "</th><th class='thirdrow'>" + Output[1] + "</th></tr>")
            }
        }
        else { $("#globalScoreBoard").html("<tr id='tr_2--1'><th>Rank</th><th>Alias</th><th>Poäng</th><th>Svårighetsgrad</th></tr>"); loadLeaderBoard() }
    }
    var insertScores = function () { for (var i = 0; i < 10; i++) { if (localStorage.getItem("score-" + i) == null) { localStorage.setItem("score-" + i, 0 + "-" + 10) } } }
    var changeKeyBindings = function (id) {
        $("#control-" + id).toggleClass("selected"); $("#desc-" + id).toggleClass("selected"); if (changeControls == !1) { $("#change-" + id).html("Avbryt"); ctrltoChange = id; menuIndex = 3 }
        else { $("#change-" + id).html("Ändra"); setBindings(!1, null); menuIndex = 2 }
        changeControls = !changeControls
    }
    var setBindings = function (changed, keyID) {
        if (changed == !0) {
            setTimeout(function () {
                if (keyID != 27 && keyID != 144 && keyID != 145 && keyID != 91 && keyID != 92 && keyID != 18 && keyID != 8 && keyID < 112 && backPressed == !1 || keyID != 27 && keyID != 144 && keyID != 145 && keyID != 91 && keyID != 92 && keyID != 18 && keyID != 8 && keyID > 123 && backPressed == !1) {
                newSettings.controls[ctrltoChange] = keyID; for (var i = 0; i < newSettings.controls.length; i++) { if (newSettings.controls[i] == keyID && ctrltoChange != i) { newSettings.controls[i] = null } }
                    changeKeyBindings(ctrltoChange); translateKeys(newSettings.controls)
                }
            }, 20)
        }
    }
    var revertToDefault = function () { modal = !0; $("#dialogBox-1").dialog({ resizable: !1, height: "auto", width: 400, close: CloseFunction, modal: !0, buttons: { "Ok": function () { localStorage.removeItem("settings"); loadSettings(!0); $(this).dialog("close") }, "Avbryt": function () { $(this).dialog("close") } } }) }
    var translateKeys = function (array) {
        for (var i = 0; i < array.length; i++) {
            if ($("#bS-" + i).length == 0) { $("#cc-" + i).html("<h2 class='buttonSelected' id='bS-" + i + "'></h2>") }
            if (array[i] >= 1 && array[i] < 4) { $("#bS-" + i).html(mouseButtons[array[i] - 1]) }
            else if (array[i] == 9) { $("#bS-" + i).html("TAB") }
            else if (array[i] == 13) { $("#bS-" + i).html("ENT") }
            else if (array[i] >= 16 && array[i] < 20) { $("#bS-" + i).html(ctrlKeys[array[i] - 16]) }
            else if (array[i] == 20) { $("#bS-" + i).html("CAPS<br>LOCK") }
            else if (array[i] == 32) { $("#cc-" + i).html("<img class='buttonIcon' src='img/spacebar.png'>") }
            else if (array[i] >= 33 && array[i] <= 40) { $("#bS-" + i).html(rightSpecialKeys[array[i] - 33]) }
            else if (array[i] == 45 || array[i] == 46) { $("#bS-" + i).html(insDel[array[i] - 45]) }
            else if (array[i] >= 96 && array[i] <= 106) { $("#bS-" + i).html("NUMP<br>" + (array[i] - 96)) }
            else if (array[i] == 106 || array[i] == 107) { $("#bS-" + i).html(numpadSymb[array[i] - 106]) }
            else if (array[i] == 109) { $("#bS-" + i).html("NUMP<br>-") }
            else if (array[i] == 111) { $("#bS-" + i).html("/") }
            else if (array[i] == 171) { $("#bS-" + i).html("+") }
            else if (array[i] == 173) { $("#bS-" + i).html("-") }
            else if (array[i] >= 192 && array[i] <= 193) { $("#bS-" + i).html(sweKeys[array[i] - 192]) }
            else if (array[i] >= 220 && array[i] < 224) { $("#bS-" + i).html(sweKeys[array[i] - 218]) }
            else if (array[i] >= 220 && array[i] < 224) { $("#bS-" + i).html("NUMP<br>ENT") }
            else if (array[i] >= 236 && array[i] < 239) { $("#bS-" + i).html(ctrlKeys2[array[i] - 236]) }
            else if (array[i] == null) { $("#bS-" + i).html("[N/A]") }
            else { $("#bS-" + i).html(String.fromCharCode(array[i]).toUpperCase()) }
        }
    }
    var toggleMenu = function (id) {
        if ($("#arrow-" + id).hasClass('enabled') == !1) { $(".setArrow").removeClass("enabled"); $(".subPanel").removeClass("enabled"); menuIndex = 1 }
        $("#arrow-" + id).toggleClass("enabled"); $("#subP-" + id).toggleClass("enabled"); if ($(".setArrow").hasClass("enabled") == !0) { menuIndex = 2 }
    }
    var update = function (deltaTime) {
        if (map[27] && hasRecPaused == !1 && menuIndex == 0 && start == !1 && gameOver == !1) { PauseGame(!0) }
        else if (map[27] && hasRecPaused == !1 && menuIndex > 0 && modal == !1) {
            hasRecPaused = !0; pauseCD(); if ($("#settingsWindow").hasClass("enabled") && menuIndex == 1 && (JSON.stringify(newSettings) != JSON.stringify(settingsStore))) { saveUnchanged() }
            else { backfunc() }
        }
        if (pause == !1 && gameOver == !1) {
            posCalc(head); var i; objCol(); gradualIncrease(); eTime += ((deltaTime) / 1000); for (i = 0; i < missile.length; i++) { posCalc(missile[i]) }
            for (i = 0; i < enemies.length; i++) { posCalc(enemies[i]); frameCol(enemies[i]) }
            playerAction(); frameCol(head); if (head.velX > 0) { head.velX = head.velX - (decay * hbr) }
            if (head.velX < 0) { head.velX = head.velX + (decay * hbr) }
            if (head.velY > 0) { head.velY = head.velY - (decay * hbr) }
            if (head.velY < 0) { head.velY = head.velY + (decay * hbr) }
            var i; for (i = 0; i < missile.length; i++) { if (missile[i].posY < -200 || missile[i].posX < -200 || missile.posY > canvas.height + 200 || missile.posX > canvas.width + 200) { missile.splice(i, 1) } }
        }
    }
    var playerAction = function () {
        if (map[kB[0]] && head.velX > -vMax * hbr) { head.velX = head.velX - (v * hbr) }
        if (map[kB[1]] && head.velX < vMax * hbr) { head.velX = head.velX + (v * hbr) }
        if (map[kB[2]] && head.velY > -vMax * hbr) { head.velY = head.velY - (v * hbr) }
        if (map[kB[3]] && head.velY < vMax * hbr) { head.velY = head.velY + (v * hbr) }
        if (map[kB[4]] && hasShot == !1) { var speed = calculateVelocity(); missile.push({ velX: speed[0] * hbr, velY: speed[1] * hbr, posX: head.posX + 35, posY: head.posY - 20, w: 100 * hbr, h: 133 * hbr, id: "NejSD", kills: "Uffe" }); hasShot = !0; coolDown(500) }
        if (map[kB[5]] && hasShot == !1) { var speed = calculateVelocity(); missile.push({ velX: speed[0] * hbr, velY: speed[1] * hbr, posX: head.posX + 35, posY: head.posY - 20, w: 100 * hbr, h: 133 * hbr, id: "NejJonas", kills: "Steffe" }); hasShot = !0; coolDown(500) }
    }
    var render = function () {
        window.requestAnimationFrame(render); currentTime = (new Date()).getTime(); deltaTime = currentTime - lastTime; lastTime = currentTime; update(deltaTime); context.clearRect(0, 0, canvas.width, canvas.height); context.drawImage(bg, 0, 0, canvas.width, canvas.width * 0.68); var i; $("#points").html(score); for (i = 0; i < enemies.length; i++) {
            if (enemies[i].id == "Steffe") { context.drawImage(SteffeImg, enemies[i].posX, enemies[i].posY, enemies[i].w, enemies[i].h) }
            else { context.drawImage(UffeImg, enemies[i].posX, enemies[i].posY, enemies[i].w, enemies[i].h) }
        }
        for (i = 0; i < missile.length; i++) {
            var missileImage; if (missile[i].id == "NejSD") { missileImage = SDimage }
            else { missileImage = Vimage }
            context.drawImage(missileImage, missile[i].posX, missile[i].posY, missile[i].w, missile[i].h)
        }
        context.drawImage(head.image, head.posX, head.posY, head.w, head.h)
    }
    var EnemyClock = function () {
        clock = window.setInterval(function () {
            createEnemies(); while (stopped > 0 && pause == !1) { stopped -= deltaTime }
            if (stopped <= 0) { stopped = 750 }
        }, stopped / diff)
    }
    var coolDown = function (timeO) { setTimeout(function () { hasShot = !1 }, timeO) }; var pauseCD = function () { setTimeout(function () { hasRecPaused = !1 }, 200) }
    var PauseGame = function (togglePause) {
        if (gameOver == !1 && togglePause == !0) { pause = !pause }
        else { pause = !1 }
        if (pause == !0) { clearInterval(clock); if (gameOver == !1 && start == !1) { postPause(0) } $("#playpause").attr({ src: "img/play.svg", title: "Fortsätt (Escape)" }) }
        else { EnemyClock(); if (start == !1 && dontResume == !1) { postResume(0) } $("#playpause").attr({ src: "img/pause.svg", title: "Pausa spelet (Escape)" }) }
        $("#pauseMenu").toggleClass("paused"); $("#mainM").addClass("enabled"); $("#rS").addClass("enabled"); hasRecPaused = !0; pauseCD()
    }
    var playMusic = function () { music.play() }
    var createEnemies = function () {
        var deg = 25 + (Math.random() * 130); deg = (deg) * (Math.PI / 180); var velH = Math.floor((Math.random() * 200) + 200); var Xcalc = (Math.cos(deg)) * velH; var Ycalc = (Math.sin(deg)) * velH; if (Ycalc < 0) { Ycalc = Ycalc * -1 }
        var rand = Math.floor(Math.random() * 4) + 1; rand += eTime / 100; if (rand >= 4) {
            var isSteffe = Math.floor(Math.random() * 2); if (isSteffe == 1) { enemies.push({ velX: Xcalc * hbr, velY: Ycalc * hbr, posX: (ww / 2) - (SteffeImg.width / 2), posY: -250, id: "Steffe", w: 150 * hbr, h: 229 * hbr, collision: !1, bounce: 1 }) }
            else { enemies.push({ velX: Xcalc * hbr, velY: Ycalc * hbr, posX: (ww / 2) - (UffeImg.width / 2), posY: -250, id: "Uffe", w: 150 * hbr, h: 210 * hbr, collision: !1, bounce: 1 }) }
        }
    }
    var posCalc = function (obj) { obj.posX = obj.posX + obj.velX * deltaTime / 1000; obj.posY = obj.posY + obj.velY * deltaTime / 1000 }
    var initializeServer = function (tries) {
        fetch('/api/initialize', { method: 'GET', headers: { 'Accept': 'application/json, text/plain', 'Content-type': 'application/json', 'cache': !1 } }).then((res) => {
            if (res.status >= 200 && res.status < 400) { return res.json() }
            else { throw (res) }
        }).then((data) => { sessionID = data.sessionID }).catch((error) => {
            if (tries <= 5) { setTimeout(() => { tries++; initializeServer(tries) }, 100) }
            else { errors++ }
        })
    }
    var frameCol = function (obj) {
        bc = obj.bounce; if (obj.collision == !1 && obj.posY >= 0) { obj.collision = !0 }
        if (obj.collision == !0) {
            if (obj.posX < 0) { obj.posX = 0; obj.velX = -obj.velX * bc; if (obj.id == "annie") { bumpSound.play() } }
            if (obj.posX > canvas.width - obj.w) { obj.posX = canvas.width - obj.w; obj.velX = -obj.velX * bc; if (obj.id == "annie") { bumpSound.play() } }
            if (obj.posY < 0) { obj.posY = 0; obj.velY = -obj.velY * bc; if (obj.id == "annie") { bumpSound.play() } }
            if (obj.posY > canvas.height - obj.h) { obj.posY = canvas.height - obj.h; obj.velY = -obj.velY * bc; if (obj.id == "annie") { bumpSound.play() } }
        }
    }
    var objCol = function () {
        var i; var j = 0; for (i = 0; i < enemies.length; i++) {
            var objX = enemies[i]; var leftX = objX.posX; var topX = objX.posY; var botX = objX.posY + objX.h; var rightX = objX.posX + objX.w; for (j = i + 1; j < enemies.length; j++) {
                var objY = enemies[j]; var leftY = objY.posX; var topY = objY.posY; var botY = objY.posY + objY.h; var rightY = objY.posX + objY.w; if (rightX < rightY && rightX > leftY && botX > topY + (c * hbr) && botX < botY || rightX < rightY && rightX > leftY && botX > botY && topX < botY - (c * hbr)) { var oldVelX = objX.velX; objX.posX = objY.posX - objX.w - 1; objX.velX = objY.velX; objY.velX = oldVelX }
                if (leftX < rightY && rightX > rightY && botX > topY + (c * hbr) && botX < botY || leftX < rightY && rightX > rightY && botX > botY && topX < botY - (c * hbr)) { var oldVelX = objX.velX; objX.posX = rightY + 1; objX.velX = objY.velX; objY.velX = oldVelX }
                if (topX < botY && topX >= botY - (c * hbr) && botX > botY && rightX > leftY && leftX < leftY || topX < botY && topX >= botY - (c * hbr) && botX > botY && leftX > leftY && leftX < rightY) { objX.posY = botY + 1; var oldVelY = objX.velY; objX.velY = objY.velY; if (objY.collision == !0) { objY.velY = oldVelY } }
                if (botX > topY && botX <= topY + (c * hbr) && botX < botY && rightX > leftY && leftX < leftY || botX > topY && botX <= topY + (c * hbr) && botX < botY && leftX > leftY && leftX < rightY) { var oldVelY = objY.velY; objX.posY = topY - objX.h - 1; objY.velY = objX.velY; if (objX.collision == !0) { objX.velY = oldVelY } }
            }
            for (j = 0; j < missile.length; j++) {
                var objY = missile[j]; var leftY = objY.posX; var topY = objY.posY; var botY = objY.posY + objY.h; var rightY = objY.posX + objY.w; if (rightX >= leftY && rightX < (rightY) && topX >= topY && topX < botY || rightX >= leftY && rightX < rightY && topY >= topX && topY < botX || leftX <= rightY && rightX > rightY && topX >= topY && topX < botY || leftX <= rightY && rightX > rightY && topY >= topX && topY < botX) {
                    if (objX.id == objY.kills) { kill(i, !0); addScore += 20 * Math.pow(diff, scorePow); postUpdateServer(0) }
                    else { objX.velX = objX.velX * 1.2 * diff; objX.velY = objX.velY * 1.2 * diff }
                    missile.splice(j, 1)
                }
            }
            if ((rightX - (c2 * hbr)) >= head.posX && rightX < (head.posX + head.w) && topX >= head.posY + (c2 * hbr) && topX < (head.posY + head.h) || (rightX - (c2 * hbr)) >= head.posX && (rightX) < (head.posX + head.w) && head.posY >= topX && head.posY < (botX - (c2 * hbr)) || leftX <= (head.posX + head.w - (c2 * hbr)) && rightX > (head.posX + head.w) && topX >= head.posY + (c2 * hbr) && topX < (head.posY + head.h) || leftX <= (head.posX + head.w - (c2 * hbr)) && rightX > (head.posX + head.w) && head.posY >= topX && head.posY < (botX - (c2 * hbr))) { life--; var blink = setInterval(function () { $("#life-" + life).toggleClass('disabled') }, 50); setTimeout(function () { clearInterval(blink); $("#life-" + life).remove() }, 500); lastHit = objX.id; kill(i, !1); if (life > 0) { sfx[4].play() } }
            if (life <= 0) { gameOver = !0; clearInterval(clock); gameOverScreen() }
        }
    }
    var gradualIncrease = function () { score = Math.floor(eTime) + Math.floor(addScore) }
    var postUpdateServer = function (tries) {
        fetch('/api/update', { method: 'POST', headers: { 'Accept': 'application/json, text/plain', 'Content-type': 'application/json' }, body: JSON.stringify({ sessionID: sessionID, diff: diff }) }).then((res) => { if (res.status > 400) { throw (res) } }).catch((error) => {
            if (tries <= 5) { setTimeout(() => { tries++; postUpdateServer(tries) }, 100) }
            else { errors++ }
        })
    }
    var postEnd = function (tries) {
        fetch('/api/end', { method: 'POST', headers: { 'Accept': 'application/json, text/plain', 'Content-type': 'application/json' }, body: JSON.stringify({ sessionID: sessionID, score: Math.floor(eTime) }) }).then((res) => { if (res.status > 400) { throw (res) } }).catch((error) => {
            if (tries <= 5) { setTimeout(() => { tries++; postEnd(tries) }, 100) }
            else { errors++ }
        })
    }
    var postPause = function (tries) {
        fetch('/api/pause', { method: 'POST', headers: { 'Accept': 'application/json, text/plain', 'Content-type': 'application/json' }, body: JSON.stringify({ sessionID: sessionID }) }).then((res) => { if (res.status > 400) { throw (res) } }).catch((error) => {
            if (tries <= 5) { setTimeout(() => { tries++; postPause(tries) }, 100) }
            else { errors++ }
        })
    }
    var postResume = function (tries) {
        fetch('/api/resume', { method: 'POST', headers: { 'Accept': 'application/json, text/plain', 'Content-type': 'application/json' }, body: JSON.stringify({ sessionID: sessionID }) }).then((res) => { if (res.status > 400) { throw (res) } }).catch((error) => {
            if (tries <= 5) { setTimeout(() => { tries++; postResume(tries) }, 100) }
            else { errors++ }
        })
    }
    var toggleDiff = function (id) { newSettings.difficulty = parseInt(id, 10); $(".difficulty").removeClass("selected"); $("#diff-" + id).addClass("selected") }
    var loadSettings = function (dontplay) {
        if (localStorage.getItem("settings") === null) { $.getJSON("js/settings.json", function (data) { return data }).then((data) => { settingsStore = data; localStorage.setItem("settings", JSON.stringify(settingsStore)); setSettings(dontplay) }) }
        else { new Promise((resolve) => { settingsStore = JSON.parse(localStorage.getItem('settings')); resolve() }).then(() => { setSettings(dontplay) }) }
    }
    var setSettings = function (exitedMenu) {
        if (settingsStore.character == undefined) { settingsStore.character = 0 }
        if (exitedMenu != undefined) { exit = exitedMenu }
        setTimeout(function () { $("#musicSlider").slider({ value: (settingsStore.musicV) * 100, max: 100, change: function () { adjustV(!1) }, orientation: "horizontal", range: "min", animate: !0 }); $("#sfxSlider").slider({ value: (settingsStore.sfxV) * 100, max: 100, change: function () { adjustV(!0) }, orientation: "horizontal", range: "min", animate: !0 }); diff = Math.floor(Math.pow((Math.sqrt(2)), settingsStore.difficulty) * 100) / 100; kB = settingsStore.controls; translateKeys(kB); $(".difficulty").removeClass("selected"); $("#diff-" + settingsStore.difficulty).addClass("selected"); $(".CharSelect").removeClass("selected"); charSel = settingsStore.character; $("#sel-" + charSel).addClass("selected"); head.image.src = "img/" + headImgs[charSel] + ".png"; head.w = headW[charSel] * hbr; $(".nrOfLives").attr("src", "img/" + headImgs[charSel] + ".png"); sfx[4] = nej[charSel]; newSettings = JSON.parse(JSON.stringify(settingsStore)); setVolume(!1) }, 100)
    }
    var setVolume = function (getSliderValue) {
        if (getSliderValue == !0) { musicVolume = newSettings.musicV; sfxVolume = newSettings.sfxV }
        else { musicVolume = settingsStore.musicV; sfxVolume = settingsStore.sfxV }
        music.volume = musicVolume; sample.volume = musicVolume; for (var i = 0; i < $(".sfx").length - 1; i++) { sfx[i].volume = sfxVolume }
    }
    var saveSettings = function (save) {
        if (save == !0) { settingsStore = JSON.parse(JSON.stringify(newSettings)); localStorage.setItem("settings", JSON.stringify(settingsStore)); setSettings(!0) }
        else { newSettings = JSON.parse(JSON.stringify(settingsStore)); setSettings(!0) }
        menuIndex = 1; backfunc()
    }
    var mainMenu = function () { scoreAlreadySubmitted = !1; start = !0; music.pause(); music.currentTime = 0; $("#pausedGame").html("Vilken politik"); $(".resume").html("Starta spelet"); $(".logo").removeClass("started"); initialValues(!0) }
    var adjustV = function (isSFX) {
        setTimeout(function () {
        newSettings.sfxV = $("#sfxSlider").slider("option", "value") / 100; newSettings.musicV = $("#musicSlider").slider("option", "value") / 100; if (isSFX == !0) {
            if (exit == !1) { kabbel.play() }
            else { exit = !1 }
        }
        else {
            if (exit == !1 && music.paused) { sample.play() }
            else { setTimeout(function () { exit = !1 }, 200) }
        }
            setVolume(!0)
        }, 50)
    }
    $(".volumeSlider").on("mousedown", function (e) { e.stopPropagation() }); $("#save").on("mousedown", function (e) { if (e.button == 0) { saveSettings(!0) } }); $(".difficulty").on("mousedown", function (e) { e.stopPropagation(); if (e.button == 0) { var id = ($(e.originalEvent.target).attr('id')).split('-')[1]; toggleDiff(id) } }); $(".CharSelect").on("mousedown", function (e) { e.stopPropagation(); if (e.button == 0) { var id = ($(e.originalEvent.target).attr('id')).split('-')[1]; toggleCharacter(id) } }); var toggleCharacter = function (id) { $(".CharSelect").removeClass("selected"); $("#sel-" + id).addClass("selected"); newSettings.character = parseInt(id) }
    var initialValues = function (gotoMenu) {
        errors = 0; gameOver = !1; if (start == !0) {
            if (gotoMenu == !1) { PauseGame(!0) }
            else { PauseGame(!1); PauseGame(!0) }
            $("#rS").removeClass("enabled")
        }
        else { pause = !1; $("#pausedGame").html("Pausat") }
        $("#mainM").addClass("enabled"); $("#pausedGame").removeClass("GO"); $("#gameOverScreen").removeClass("enabled"); head.posX = ((canvas.width) / 2) - (head.w) / 2; head.posY = (canvas.height) / 2.2; head.velX = 0; head.velY = 0; missile = []; enemies = []; hasShot = !1; score = 0; eTime = 0; life = 3; addScore = 0; $("#lives").html(""); selected = 0; for (var i = 0; i < life; i++) { $("#lives").append("<img class='nrOfLives' id='life-" + i + "' src='img/" + headImgs[charSel] + ".png'>") }
    }
    var kill = function (i, missileHit) {
        if (enemies[i].id == "Steffe" && missileHit == !0) { kabbel.play() }
        else if (enemies[i].id == "Uffe" && missileHit == !0) { felSak.play() }
        enemies.splice(i, 1)
    }
    var calculateVelocity = function () {
        var velXY = [2]; var centerX = head.posX + head.w / 2; var centerY = head.posY + head.h / 2; var dX = centerX - mousePos[0]; var dY = centerY - mousePos[1]; if (dX == 0) { dX = 0.00001 }
        var angle = Math.atan2(dY, dX); velXY[0] = Math.cos(angle) * mS; velXY[1] = Math.sin(angle) * mS; return velXY
    }
    var gameOverScreen = function () {
        var second; $("#pauseMenu").addClass("paused"); $("#mainM").removeClass("enabled"); $("#playpause").removeClass("enabled"); $("#pausedGame").addClass("GO"); $("#gameOverScreen").addClass("enabled"); $("#rS").removeClass("enabled"); if (lastHit == "Steffe") { second = "Jonas"; $("#conseq").html("Du blev del av regeringen du ville byta ut!") }
        else { second = "Jimmie"; $("#conseq").html("Du bröt vallöftet att inte samarbeta med SD!") }
        $("#pausedGame").html("Du bildade en regering med " + lastHit + " och " + second + "!"); $("#finalResult").html(score); price.play(); if (hasRecentlySaved == !1) { localScoreSave(); hasRecentlySaved = !0; setTimeout(function () { hasRecentlySaved = !1; postEnd(0) }, 500) }
    }
    var localScoreSave = function () {
        if (localStorage.getItem("score-0") == null) { insertScores() }
        for (var i = 0; i < 10; i++) {
            if (localStorage.getItem("score-" + i).split('-')[0] < score && localStorage.getItem("score-" + i) != null) {
                var oldScore; var pushScore; for (var j = i + 1; j < 10; j++) {
                    if (oldScore == undefined) { oldScore = localStorage.getItem("score-" + j); pushScore = localStorage.getItem("score-" + i) }
                    else { pushScore = oldScore }
                    oldScore = localStorage.getItem("score-" + j); if (j <= 9) { localStorage.setItem("score-" + j, pushScore) }
                    localStorage.setItem("score-" + i, score + "-" + (settingsStore.difficulty))
                }
                i = 10
            }
        }
    }
    var setSize = function (canvas) {
        if (sizeSet == !1) {
            hbr = ww / 1800; canvas.width = ww; canvas.height = wh - 55; sizeSet = !0; head.w = headW[charSel] * hbr; head.h = 197 * hbr; head.velX = head.velX * hbr; head.velY = head.velY * hbr; head.posX = head.posX * hbr; head.posY = head.posY * hbr; if (missile != undefined) { for (i = 0; i < missile.length; i++) { missile[i].w = 100 * hbr; missile[i].h = 133 * hbr; missile[i].velX = missile[i].velX * hbr; missile[i].velY = missile[i].velY * hbr; missile[i].posX = missile[i].posX * hbr; missile[i].posY = missile[i].posY * hbr } }
            if (enemies != undefined) { for (i = 0; i < enemies.length; i++) { enemies[i].w = enemies[i].w * hbr; enemies[i].h = enemies[i].h * hbr; enemies[i].velX = enemies[i].velX * hbr; enemies[i].velY = enemies[i].velY * hbr; enemies[i].posX = enemies[i].posX * hbr; enemies[i].posY = enemies[i].posY * hbr } }
            setTimeout(function () { sizeSet == !1 }, 100)
        }
        if(ww <= 900&&ww>wh||ww<=900&&wh < 450){
            $(".mainArea").addClass("wide");
        }
        else {
            $(".mainArea").removeClass("wide");
        }
    }
    var EnterSettings = function () { menuIndex = 1; $("#settingsWindow").addClass("enabled"); $("#mainM").removeClass("enabled"); $("#rS").removeClass("enabled") }
    var saveUnchanged = function () { modal = !0; $("#dialogBox-0").dialog({ resizable: !1, height: "auto", width: 400, close: CloseFunction, modal: !0, buttons: { "Spara": function () { saveSettings(!0); $(this).dialog("close"); modal = !1 }, "Ignorera": function () { saveSettings(!1); save = !1; modal = !1; $(this).dialog("close") }, "Avbryt": function () { $(this).dialog("close"); modal = !1 } } }) }
    var EnterAbout = function () { menuIndex = 1; $("#aboutWindow").addClass("enabled"); $("#mainM").removeClass("enabled"); $("#rS").removeClass("enabled") }
    var backfunc = function () {
        if (menuIndex == 1) {
            $(".setArrow").removeClass("enabled"); $(".subPanel").removeClass("enabled"); $("#settingsWindow").removeClass("enabled"); $("#aboutWindow").removeClass("enabled"); $("#scoreBoard").removeClass("enabled"); if (start == !1 && gameOver == !1) { $("#rS").addClass("enabled") }
            if ($("#submitWrapper").hasClass("enabled")) { closeSubmit() }
            else { $("#mainM").addClass("enabled") }
            menuIndex--
        }
        else if (menuIndex == 2) { $(".setArrow").removeClass("enabled"); $(".subPanel").removeClass("enabled"); menuIndex-- }
        else if (menuIndex == 3) { changeKeyBindings(ctrltoChange) }
    }
    return { init: function (id) { canvas = document.getElementById(id); setSize(canvas); loadSettings(!1); context = canvas.getContext('2d'); $(canvas).focus(); head.image = new Image(); head.image.src = "img/" + headImgs[charSel] + ".png"; head.collision = !0; head.bounce = 0.6; head.id = "annie"; if(isMobile.any == true){$("#panel-2").css("display","none");} initialValues(!1); lastTime = (new Date()).getTime(); render() } }
}